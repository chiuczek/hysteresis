#ifndef __chiu_utility__
#define __chiu_utility__
#pragma once

#include <limits>
#include <cmath>
#include <fstream>
#include <iostream>

using namespace std;

const double epsilon = numeric_limits<double>::epsilon();
const double sqrteps = sqrt(epsilon);

const double TINY = 1e-20;

const char sep = ',';
const char el = '\n';

//	Some generally useful constants, typedefs and inline
//	function definitions for use in loadsa places.

const double PI = 3.141592653589793;
const double rt2 = 1.414213562373095;

typedef double (*pfn1)(double);
typedef double (*pfn2)(double,double);

inline double Max(const double x,const double y)
{
	return ( x >= y ? x : y );
};

inline double Min(const double x,const double y)
{
	return ( x >= y ? y : x );
};

inline int sgn(const double val)
{
	return (val>=0)?1:-1;
};

template<class C>
inline double intPow(C x, int N)
{
	if(N == 0)
		return 1;
	if(N < 0)
		return 1./intPow(x,-N);
	return x*intPow(x,N-1);
};

inline double retZero(double t)
{
	return 0;
};

inline double oneFn(double t)
{
	return t;
};

inline double retZero(double a,double b)
{
	return 0;
};

template<class fnClass>
class rkBridge
{
public:
	rkBridge(const double& _dep, const double& _ind, fnClass &_f) 
		: dep_cur(_dep), ind_cur(_ind), f_int(_f) { };
	~rkBridge(void) {};

	double get_dep() { return dep_cur; };
	double get_ind() { return ind_cur; };

	double ret_dep() { return dep_res; };
	double ret_ind() { return ind_res; };

	void set_dep( double _dep ) { dep_res = _dep; };
	void set_ind( double _ind ) { ind_res = _ind; };

	void update( double _dep, double _ind ) { dep_cur = _dep; ind_cur = _ind; };

	double f(double _dep, double _ind) { return f_int(_dep,_ind); };

protected:
	double dep_cur;
	double ind_cur;
	double dep_res;
	double ind_res;
	fnClass &f_int;
};

template<class fnClass>
void rk4Int(double _h, rkBridge<fnClass> &pars)
{
	double yold, told, k1, k2, k3, k4, h2 = 0.5*_h;
	double ynew, tnew;
	yold = pars.get_dep();
	told = pars.get_ind();

	k1 = pars.f( yold, told );
	k2 = pars.f( yold + h2*k1, told + h2 );
	k3 = pars.f( yold + h2*k2, told + h2 );
	k4 = pars.f( yold + _h*k3, told + _h );

	ynew = yold + (_h/6.0)*(k1 + 2*k2 + 2*k3 + k4);
	tnew = told + _h;

	pars.set_dep(ynew);
	pars.set_ind(tnew);
};

template<class fn_obj>
class root_bridge
{
public:
	root_bridge(fn_obj& _fn) :fn(_fn) {};
	~root_bridge(void) {};
	inline void bind_val(const double &_res) { res = _res; };
	inline double operator()(const double &_x) { return fn(_x) - res; };
private:
	fn_obj& fn;
	double res;
};

//	Function to find the solution (x) of _fn(x) = des_res which lies between 
//	guess_1 and guess_2, to a tolerance which may be specified.
template<class fn_obj>
double bisect(double des_res, double guess_1, double guess_2, fn_obj &_fn, double tol = 1e-9)
{
	double x_l, x_r, x_mid, y_l, y_r, y_mid;
	int i = 0;
	root_bridge<fn_obj> fn(_fn);
	fn.bind_val(des_res);
	
	x_l = Min(guess_1,guess_2);
	y_l = fn(x_l);
	x_r = Max(guess_1,guess_2);
	y_r = fn(x_r);

	if( !(y_l*y_r <= 0) )
	{
		cerr << "Error in bisection method!" << el;
		cerr << "Root not bracketed by " << x_l << " and " << x_r << el;
		throw(1);
	}

	while( fabs(y_l - y_r) >= tol )
	{
		x_mid = 0.5*(x_r+x_l);
		y_mid = fn(x_mid);

		if( y_l*y_mid <= 0 )
		{
			x_r = x_mid;
			y_r = y_mid;
		}
		else
		{
			x_l = x_mid;
			y_l = y_mid;
		}

		if( ++i > 100 )
		{
			cerr << "Error in bisection method!" << el;
			cerr << "Failed to converge after 100 iterations" << el;
			throw(1);
		}
	}
	return 0.5*(x_r+x_l);
};

#endif