//	#include "rkStep.h"
#include <cmath>
#include <vector>
#include <iostream>
#include <fstream>
#include "utility.h"

double simpTrapInt(pfn1 integrand, const double left, const double right, const double step);
double trapInt(pfn1, const double, const double, const int);
void polint(vector<double>&, vector<double>&, const double, double&, double&);
double rombInt(pfn1 integrand, const double left, const double right, const double eps = 1.0e-10);
//double rkInt(pfn2 integrand, double left, double right, double eps);