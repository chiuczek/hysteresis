/************************************************************

	Code for the csvParser class, declared in csvParser.h
	Reads a comma separated volume (CSV) formatted file
	and stores the data contained for use in programs.
	It will ignore rows in the input file which begin with
	a # symbol, which may be used for comments/extra data
	of which the program need not know.

	See csvParser for interface details.

************************************************************/

#include "csvParser.h"

csvParser::csvParser()
	: errorSwitch(0)
	, fileRows(0)
	, fileColumns(0)
	, isDataParsed(false)
{
}

csvParser::~csvParser()
{
	if(isDataParsed)
	{
		for(int i = 0; i < fileRows; i++)
			delete [] fileNumbers[i];
		delete [] fileNumbers;
	}
}

double csvParser::number(int i, int j)
{
	return fileNumbers[i][j];
}

int csvParser::noOfRows()
{
	if(errorSwitch != 1)
		return fileRows;
	else
		return -1;
}

int csvParser::noOfColumns()
{
	if(errorSwitch != 1)
		return fileColumns;
	else
		return -1;
}

void csvParser::getInfo(int end)
{
	int pos, colMax = 0, i = 0, rows = 0, columns = 0;
	char buff[200];

	pos = inFile.tellg();
	if( pos < 0 )
	{
        cerr << "Error reading file!" << endl;
        return;
	}

	while(1)	//Infinite loop will be broken when eof is reached.
	{
		//Take a line from the file, store it temporarily in buff
		inFile.getline(buff,199,'\n');

		//Check to see if line is blank or a comment
		if(buff[0] != '#' && buff[0] != '\0')
		{
			//If not then increment no. of rows.
			rows++;

			//And check no. of columns (every four rows)
			if( (rows-1)%4 == 0 )
			{
				i = 0;
				columns = 1;
				while(buff[i] != '\0')
				{
					if(buff[i] == ',')
						columns++;
					i++;
				}
				if(columns > colMax)
					colMax = columns;
			}
		}

		//Check if end of file has been reached.
		pos = inFile.tellg();
		if(pos >= end)
			break;
	}

    inFile.seekg(0, ios_base::beg);

    if( inFile.rdstate() != ios_base::goodbit )
    {
        inFile.clear();
    }

	fileRows = rows;
	fileColumns = columns;
}

void csvParser::fillNumbers(int end)
{
	int pos,row = 0,column = 0,i,j,split;
	char buff[200];
	char nums[50];

	while(1)
	{
		//Take a line from the file, store it temporarily in buff
		inFile.getline(buff,199,'\n');
		pos = inFile.tellg();
        if( pos < 0 )
        {
            cerr << "Error reading file!" << endl;
            return;
        }

		i = 0;

		//Check to see if line is blank or a comment
		if(buff[0] != '#' && buff[0] != '\0')
		{
			split = i = j = 0;

			//Scan each line, putting the characters from buff into num
			while(1)
			{
				//Take it a column at a time
				if(buff[j] != ',' && buff[j] != '\0')
				{
					nums[j-split] = buff[j];
					j++;
				}
				else
				{
					//If there's a break of some sort then interpret and move on
					nums[j-split] = '\0';

					fileNumbers[row][i] = atof(nums);

					//If the break is a line break then go to a new row
					if(buff[j] == '\0')
						break;

					j++;
					split = j;
					i++;
				}
			}
			row++;
		}

		//Check if end of file has been reached.
		pos = inFile.tellg();
		if(pos >= end)
			break;
	}
}

void csvParser::parse(const char* fileName)
{
	int end;

	if(errorSwitch == -1)
		return;

	//Open the file as an input stream
	inFile.open(fileName,ios_base::in);
	//Test for errors
	if(inFile.is_open() == 0)
	{
		fprintf(stderr,"File error!\n");
		errorSwitch = 1;
		return;
	}

	//Marks the end point of the file (for testing if eof is reached later)
	inFile.seekg(0, ios_base::end);
	end = inFile.tellg();
	inFile.seekg(0, ios_base::beg);

	getInfo(end);
	int pos = inFile.tellg();

	//Allocate memory for the data
	fileNumbers = new double*[fileRows];
	for(int i = 0; i < fileRows; i++)
		fileNumbers[i] = new double[fileColumns];

	fillNumbers(end);

	errorSwitch = -1;

	inFile.close();
	isDataParsed = true;
}

int csvParser::fileError(void)
{
	if(errorSwitch == 1)
		return 1;
	else
		return 0;
}
