#include <iostream>
#include <fstream>
#include <cmath>
#include <ctime>
#include <sstream>
#include <string>
#include <conio.h>
#include "PrDEsolver.h"
#include "absPreisach.h"
#include "infuncs.h"

using namespace std;

double stod(string);


int main()
{
	unPreisach state(-1.0,1.0);

	cout << "Welcome to the Preisach-differential Equation Solver!\n\n";
	cout <<	"This program will generate numerical solutions to the system of equations\n";
	cout << "\t\t y'(t) = k(f(t)-x(t))\n";
	cout << "\t\t  y(t) = (Px)(t)\n";
	cout << "\t\t  f(t) = sin(wt)\n\n";
	cout << "Please enter the coefficient of the system, k:\n>  ";
	cin >> inFuncs::k;
	cout << "Now enter the frequency of the input, w:\n>  ";
	cin >> inFuncs::freq;
	cout << "Parameters entered, proceeding with calculations...\n";

	double maxT = PI*60/inFuncs::freq;

	time_t now;
	time(&now);
	struct tm * time = localtime(&now);
	char timeStamp[30];
	strftime(timeStamp,30,"%M.%S",time);
	
	ofstream tout;
	ostringstream fileName;
	fileName << "PrDE.k" << inFuncs::k << ".w" << inFuncs::freq
		<< "." << timeStamp << ".csv";
	tout.open(fileName.str().c_str());
	if(!tout.is_open())
	{
		cerr << "File error!\n";
		cerr << "Check that disk is not full or write-protected, or that ";
		cerr << fileName.str() << " is not open or in use.";
		return -1;
	}
	
	PrDEsolver solver(&state,state.output(),0,1e-5,1e-7,inFuncs::F,inFuncs::dFdt,inFuncs::dFdy);

	clock_t start, finish;

	double tInside = 0.0;
	int progress = 0;
	int i = 0;
	int maxSec = 600;

	start = clock();
	cout << "  0% completed";
	while(tInside < maxT)
	{
		solver.step();
		tInside = solver.tOut();
		i++;
		if( i % 20 == 0 )
		{
			progress = int( 100.0*tInside/maxT );
			cout << "\b\b\b\b\b\b\b\b\b\b\b\b\b\b"; 
			cout.width(3);
			cout << right << progress << "% completed";
		}
		tout << tInside << sep << inFuncs::in(tInside) << sep
			<< solver << endl;
	}
	finish = clock();
	cout << "\b\b\b\b\b\b\b\b\b\b\b\b\b\b";
	cout << "100% completed\n";

	cout << "\nEndpoint reached in " << i << " iterations.\n";
	cout << "Calculation time " << (finish-start)/(1.0*CLOCKS_PER_SEC) << " seconds.\n";
	cout << inFuncs::funcCounter << " function evaluations required.\n";
	cout << "Results stored in " << fileName.str() << endl;

	cout << "\nPress any key to continue...";
	_getch();
}



double stod(string s)	//	Very rough string to double converter, no checking of anything!
{
	stringstream buff;
	buff << s;
	double res;
	buff >> res;
	return res;
}

