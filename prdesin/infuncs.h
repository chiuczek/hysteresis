#include <cmath>

using namespace std;

namespace inFuncs
{

double freq;
double k;

int funcCounter = 0;

inline double in(const double t)
{
	return sin(freq*t);
}

inline double dint(const double t)
{
	return freq*cos(freq*t);
}

inline double F(const double t,const double y)
{
	funcCounter++;
	return -k*(y - in(t));
};

inline double dFdt(const double t,const double y)
{
	return k*dint(t);
};

inline double dFdy(const double t,const double y)
{
	return -k;
};

}