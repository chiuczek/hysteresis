#ifndef __chiu_PrDE_solver__
#define __chiu_PrDE_solver__

#pragma once
//#include "rkstep.h"
#include "utility.h"
#include "absPreisach.h"
#include <iostream>
#include <sstream>
#include <fstream>

class PrDEsolver
{
private:
	double y,t,ynew,yerr,ylast,eps,yscal,tauLast,tauNext,tauOrg,dydt;
	static double tauMax;
	static double tauMin;

	absPreisach *ptState;
	absPreisach &state;
	double yJump;
	pfn2 f;
	pfn2 dfdy;
	pfn2 dfdt;
	int dir;
	enum stepType {init,normal,linear,toWipe,toLin};
	stepType nextStep;
	stepType lastStep;
	static double tauLinStep;

	void rkInt(double h);
	double right(double _t, double _y);
	double right();
	double linKappa();
	double linCoeffA();
	double linCoeffB();

	static char separator;
	void linStep();
	void preWipeStep();
	void preLinStep();
	void normStep();
	int yDirection();

	ofstream dout;

public:
	PrDEsolver(absPreisach *state0,const double y0,const double t0,const double tau0,
		const double acc,const pfn2 rhsFn,const pfn2 rhsDt,const pfn2 rhsDy)
			:y(y0), t(t0), tauLast(tau0), tauNext(tau0), tauOrg(tau0), eps(acc)
			, ptState(state0), f(rhsFn), dfdy(rhsDy), dfdt(rhsDt), ylast(y0)
			,state(*state0), nextStep(normal), lastStep(init)
	{
		state.add(y);
		dir = yDirection();
		if( state.isIncreasing() != dir )
			nextStep = linear;
		yJump = state.nextWipe();
		yscal = Max(fabs(state.getMin()),fabs(state.getMax()));
		if(fabs(right(t,y)) < eps)
			nextStep = linear;
		dout.open("step info.txt");
	};
	~PrDEsolver(void) {};

	void step();
	inline friend ostream& operator<<(std::ostream &out, const PrDEsolver &prde)
	{
		ostringstream temp;
		temp.precision(14);
		temp << prde.state.output() << prde.separator
			<< prde.y << prde.separator
			<< prde.state.deri() << prde.separator;

/*		temp << prde.lastStep << prde.separator
			<< prde.t << prde.separator
			<< prde.f(prde.t,prde.y) << prde.separator
			<< prde.dfdt(prde.t,prde.y) << prde.separator
			<< prde.f(prde.t,prde.y)/prde.state.deri() << prde.separator
			<< prde.separator
			<< prde.y << prde.separator 
			<< prde.state.output() << prde.separator
			<< prde.state.deri() << prde.separator;*/
		return (out << temp.str());
	};

	inline void setDataSeparator(const char c = ',');
	bool isStepNorm() const
	{
		bool res = (lastStep == normal);
		return res;
	};
	inline double tOut() const
	{
		return t;
	}
};

#endif