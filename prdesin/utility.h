#ifndef __chiu_utility__
#define __chiu_utility__
#pragma once

#include <limits>
#include <cmath>


using namespace std;

const double epsilon = numeric_limits<double>::epsilon();
const double sqrteps = sqrt(epsilon);

const double TINY = 1e-20;

const char sep = ',';

//	Some generally useful constants, typedefs and inline
//	function definitions for use in loadsa places.

const double PI = 3.141592653589793;
const double rt2 = 1.414213562373095;

typedef double (*pfn1)(double);
typedef double (*pfn2)(double,double);

inline double Max(const double x,const double y)
{
	return ( x >= y ? x : y );
};

inline double Min(const double x,const double y)
{
	return ( x >= y ? y : x );
};

inline int sgn(const double val)
{
	return (val>=0)?1:-1;
};

template<class C>
inline double intPow(C x, int N)
{
	if(N == 0)
		return 1;
	if(N < 0)
		return 1./intPow(x,-N);
	return x*intPow(x,N-1);
};

inline double retZero(double t)
{
	return 0;
};

inline double oneFn(double t)
{
	return t;
};

inline double retZero(double a,double b)
{
	return 0;
};



#endif