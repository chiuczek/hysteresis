//
//  main.cpp
//  fillnint
//  
//  Calculate energy dissipation from reconstructed data...
//
//  Created by Hugh McNamara on 21/06/2012.
//  Copyright (c) 2012 Mathematical Institute, University of Oxford. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <string>

#include "soilPreisach.h"
#include "csvParser.h"
#include "utility.h"

int main()
{
  //  hysteresis object
  soilPreisach state(-10.0,1.0);
  //  input data parser
  csvParser data;
  
  //  get input file name and parse into memory
  string inFile;
  cout << "Please enter the input file name: ";
  cin  >> inFile;
  cout << "Thank you!" << el;
  data.parse(inFile.c_str());
  if( data.fileError() )
  {
    cerr << "Error parsing data file!" << endl;
    return -1;
  }
  //  input file has format: time, moisture, matric... (further values not needed)
  //  soilPreisach object needs transformed matric potential values, using
  //  x = 1 + 4y, where y is the physical matric potential value
  //  data file supplies x, while energy calculation requires y
  
  //  set up output file, precision and header
  ofstream fout;
//  inFile += ".erg.csv";
  fout.open((inFile+".erg.csv").c_str());
  fout.precision(14);
  cout.precision(14);
  fout << "#Point No." << sep << "Moisture" << sep << "Potl" << sep;
  fout << "dE" << sep << "E" << sep << "E/t" << el;
  fout << data(0,0) << sep << data(0,1) << sep << 0.25*(data(0,2)-1.0) << sep;
  fout << 0 << sep << 0 << sep << 0 << el;
  
  ofstream allout;
  allout.open((inFile+".all.csv").c_str());
  allout.precision(14);
  
  int intpts = 101;  // no of fill-in pts
  double intsum, ergsum, ergrate, derg;
  double leftQ, leftPsi, rightQ, rightPsi, leftT, rightT;
  double dQ, dx, incPsi;
  double leftx, rightx, incx;
  double curPsi, lastPsi;
  double curQ, lastQ;
  double curx;
  
  int ndat = data.noOfRows();
  int ncol = data.noOfColumns();
  
  ergsum = 0.0;
  
  for( int ii = 1; ii < ndat; ii++ )    //  start at one to cover every pair of adjacent data
  {
    leftT     = data(ii-1,0);
    rightT    = data(ii,0);
    leftQ     = data(ii-1,1);
    rightQ    = data(ii,1);
    leftx     = data(ii-1,2);
    rightx    = data(ii,2);
    dx        = rightx - leftx;
    incx      = dx/(intpts-1.0);
    
    //  change to Psi...
    leftPsi   = 0.25*(leftx-1.0);
    rightPsi  = 0.25*(rightx-1.0);
    incPsi    = 0.25*incx;
    
    intsum = 0.0;
    
    state.add( leftx );
    if( fabs( state.output(rightx) - rightQ ) > 1e-6 )
    {
      cerr << "Possible error at point " << ii << ": mismatch in input/output of\n";
      cerr << "hysteresis object.\n";
      cerr << state.output(rightx) << " != " << rightQ << endl;
    }
    intsum = 0.0;
    curPsi = lastPsi = leftPsi;
    curQ = lastQ = leftQ;
    curx = leftx;
    
    for( int jj = 0; jj < intpts; jj++ )
    {
      curx += incx;
      curPsi += incPsi;
      //      state.add( curx );
      curQ = state.output( curx );
      
      allout << curPsi << sep << curQ << el;
      
      intsum += 0.5*(curPsi+lastPsi)*(curQ-lastQ);
      
      lastQ = curQ;
      lastPsi = curPsi;
    }
    
    derg = intsum;
    ergsum += derg;
    ergrate = ergsum/rightT;
    
    fout << rightT << sep << rightQ << sep << rightPsi << sep << derg << sep;
    fout << ergsum << sep << ergrate << el;
  }
  
  fout.close();
  allout.close();
}