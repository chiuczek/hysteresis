#pragma once

const double hg = 24.3723;
const double nm = 4.47878;
const double mm = 0.55345;
const double mpow = -1.0*(mm+1);
const double qs = 0.27237;
const double q0 = 0.008222030620874518;
const double c = 0.4152036488094558;
const double fn = 4.067362460553633e7;
const double A = (fn*mm*nm*qs)/((1.0-c)*hg*hg*(qs-q0));

double triPar;

double baseMeas(double alpha)
{
	double x = (1.0-alpha)/hg;
	return A * pow(x,nm-2) * pow(1 + fn*pow(x,nm),mpow);
}

double integrand1(double alpha)
{
	return baseMeas(alpha);
}

double integrand2(double alpha)
{
	return (triPar-alpha)*baseMeas(alpha);
}

double integrand3(double alpha)
{
	return (alpha-1.0)*baseMeas(alpha);
}