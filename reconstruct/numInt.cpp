#include "numInt.h"

using namespace std;

double simpTrapInt(pfn1 integrand, const double left, const double right, const double step)
{
	double sum, dx, interval;
	int n;

	interval = right - left;
	n = int(interval/step);
	dx = interval/(1.0*n);

	if( fabs(right - left) < 1e-9 )
		return 0.0;

	sum = 0.5*(integrand(left)+integrand(right));

	for(int i = 1; i < n; i++)
	{
		sum += integrand(left + i*dx);
	}
	sum *= dx;

	return sum;
}

double trapInt(pfn1 integrand, const double left, const double right, const int n)
{
	double x, tnm, sum, dx;
	static double s;
	int it,j;

	if(n == 1)
		return (s = 0.5*(right - left)*(integrand(left)+integrand(right)));
	else
	{
		for(j = 1, it = 1; j < n-1; j++)
			it <<= 1;
		tnm = it;
		dx = (right - left)/tnm;
		x = left + 0.5*dx;
		for( sum = 0.0, j = 0; j < it; j++, x+= dx)
			sum += integrand(x);
		s = 0.5*(s + (right - left)*sum/tnm);
		return s;
	}
}

void polint(vector <double> &xi, vector <double> &yi, const double x, double &y, double &dy)
{
	int ns = 0;
	double den, dif, dift, ho, hp, w;

	int n = xi.size();
	vector <double> c(n);
	vector <double> d(n);

	dif = fabs(x - xi[0]);
	for(int i = 0; i < n; i++)
	{
		if( (dift = fabs(x-xi[i])) < dif )
		{
			ns = i;
			dif = dift;
		}
		c[i] = yi[i];
		d[i] = yi[i];
	}
	y = yi[ns--];

	for(int m = 1; m < n; m++)
	{
		for(int i = 0; i < n-m; i++)
		{
			ho = xi[i] - x;
			hp = xi[i+m] - x;
			w = c[i+1] - d[i];
			if( (den = ho - hp) == 0.0 )
			{
				cerr << "Error in polynomial interpolation!";
				throw(0);
			}
			den = w/den;
			d[i] = hp*den;
			c[i] = ho*den;
		}
		y += (dy = (2*(ns+1) < (n-m) ? c[ns+1] : d[ns--]));
	}
}

double rombInt(pfn1 integrand, const double left, const double right, const double eps)
{
	const int JMAX = 20, JMAXP = JMAX + 1, K = 5;
	double ss, dss;
	vector <double> s(JMAX);
	vector <double> h(JMAXP);
	vector <double> s_t(K);
	vector <double> h_t(K);

	h[0] = 1.0;

	for(int j = 1; j <= JMAX; j++)
	{
		s[j-1] = trapInt(integrand,left,right,j);
		if( j >= K )
		{
			for(int i = 0; i < K; i++)
			{
				h_t[i] = h[j-K+i];
				s_t[i] = s[j-K+i];
			}
			polint(h_t,s_t,0.0,ss,dss);
			if(fabs(dss) <= eps*fabs(ss) || (fabs(ss) <= eps && j >= 3))
				return ss;
		}
		h[j] = 0.25*h[j-1];
	}
	cerr.precision(13);
	cerr << "Too many steps in routine rombInt!" << endl;
	cerr << "rombInt called from " << left << " to " << right << endl;
//	throw(0);
	return 0.0;
}
/*
double rkInt(pfn2 integrand, double left, double right, double eps)
{
	if( fabs(left - right) < eps )
		return 0;
	rkSolver inter(1,left,1e-5,eps,integrand);
	double tCur = inter.curT(), yCur = inter.curY();
	double tLast, yLast;
	while(tCur < right)
	{
		tLast = tCur;
		yLast = yCur;
		inter.step();
		tCur = inter.curT();
		yCur = inter.curY();
	}
	//	Now have two (t,y) pairs, one on either side of the
	//	right-hand limit of integration. Use linear interpolation
	//	for the final result.
	double res = yLast + ((yCur-yLast)/(tCur-tLast))*(right-tLast);
	return res - 1;
}
*/