#include "soilPreisach.h"
#include "utility.h"
#include <iostream>
#include <fstream>

int main() {
  soilPreisach state(-20.0,1.0);
  ofstream fout;
  fout.open("curvediag2.csv");
  fout.precision(14);
  
  double u = -20.0;
  double du = 0.005;
  double v;
  int i = 0;
  double u1 = -0.05;
  double u2 = 0.35;
  double u3 = -2.0;
  double u4 = 0.6;

  double utop = u3, ubot = u3;
  state.add(-20.0);
  state.add(u4);
  state.add(u1);
  v = state.output(u2);
  state.add(1.0);
  state.add(u3);
  double vt = state.output(u2);
  if( vt > v ) {
    cerr << "Error, wrong side!";
    return -1;
  }
  while( vt < v ) {
    ubot = utop;
    utop += 0.1;
    state.add(1.0);
    state.add(utop);
    vt = state.output(u2);
  }
  // value bracketed, now converge
  double err = fabs( vt - v );
  double umid;
  while ( err > 1.0e-6 ) {
    umid = 0.5*(ubot + utop);
    state.add(1.0);
    state.add(umid);
    vt = state.output(u2);
    if( vt > v ) {
      utop = umid;
    }
    else {
      ubot = umid;
    }
    err = fabs( vt - v );
  }
  u3 = umid;

  // output file header
  fout << "#, u, v" << el;

  // first curve - main wetting (inc from left boundary to right boundary)
  for( u = -20.0; u <= 1.0; u += du ) {
    v = state.output(u,1);
    fout << i << sep << u << sep << v << el;
    i++;
  }
  fout << el << el;

  // next - main drying (dec from right boundary to left)
  for( u = 1.0; u >= -20.0; u -= du ) {
    v = state.output(u,1);
    fout << i << sep << u << sep << v << el;
    i++;
  }
  fout << el << el;

  // first minor loop - from main wetting curve
  state.add(-20.0);
  state.add(u2);
  for( u = u2; u >= u1; u -= du ) {
    v = state.output(u,1);
    fout << i << sep << u << sep << v << el;
    i++;
  }
  for( u = u1; u <= u2; u += du ) {
    v = state.output(u,1);
    fout << i << sep << u << sep << v << el;
    i++;
  }
  fout << el << el;

  // second minor loop - from main drying curve
  state.add(1.0);
  state.add(u1);
  for( u = u1; u <= u2; u += du ) {
    v = state.output(u,1);
    fout << i << sep << u << sep << v << el;
    i++;
  }
  for( u = u2; u >= u1; u -= du ) {
    v = state.output(u,1);
    fout << i << sep << u << sep << v << el;
    i++;
  }
  fout << el << el;

  // a third minor loop will be formed both from a primary wetting curve
  // and from a primary drying curve. Start with the wetting curve
  state.add(1.0);
  state.add(u3);
  for( u = u3; u <= u2; u += du ) {
    v = state.output(u,1);
    fout << i << sep << u << sep << v << el;
    i++;
  }
  fout << el << el;
  // then add the loop itself
  for( u = u2; u >= u1; u -= du ) {
    v = state.output(u,1);
    fout << i << sep << u << sep << v << el;
    i++;
  }
  for( u = u1; u <= u2; u += du ) {
    v = state.output(u,1);
    fout << i << sep << u << sep << v << el;
    i++;
  }
  fout << el << el;
  // and now add the drying curve
  state.add(-20.0);
  state.add(u4);
  for( u = u4; u >= u1; u -= du ) {
    v = state.output(u,1);
    fout << i << sep << u << sep << v << el;
    i++;
  } 

  fout.close();
}

