#include "soilPreisach.h"
#include "utility.h"
#include <iostream>
#include <fstream>

int main() {
  soilPreisach state(-20.0,1.0);
  ofstream fout;
  fout.open("curvediag2.csv");
  fout.precision(14);
  
  double u = -20.0;
  double du = 0.005;
  double v;
  int i = 0;
  double u1 = -1.0;
  double u2 = 0.35;

  // output file header
  fout << "#, u, v" << el;

  // first curve, main wetting
  for( u = -20.0; u <= 1.0; u += du ) {
    v = state.output(u,1);
    fout << i << sep << u << sep << v << el;
    i++;
  }
  fout << el << el;

  // second curve, main drying
  for( u = 1.0; u >= -20.0; u -= du ) {
    v = state.output(u,1);
    fout << i << sep << u << sep << v << el;
    i++;
  }
  fout << el << el;
  
  // third curve, primary wetting, branches from main drying at u1
  state.add(1.0);
  state.add(u1);
  for ( u = u1; u < 1.0; u += du ) {
    v = state.output(u,1);
    fout << i << sep << u << sep << v << el;
    i++;
  }
  fout << el << el;

  // final curve, secondary drying, branches from primary wetting at u2
  state.add(1.0);
  state.add(u1);
  state.add(u2);
  for ( u = u2; u > -20.0; u -= du ) {
    v = state.output(u,1);
    fout << i << sep << u << sep << v << el;
    i++;
  }
  
  fout.close();
}

