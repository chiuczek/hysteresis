#include "soilPreisach.h"
//#include "poulovassilis1.h"
#include "utility.h"
#include "csvParser.h"
#include <iostream>
#include <fstream>
#include <ctime>
#include <string>

/*	Note:	The input variable for the soilPreisach class is transformed with respect to
			the physical variable involved. Denoting by x the input used in this program,
			and by y the physical variable (matric potential, in metres), the relationship
			is: y = (x-1)/4																	*/

int main()
{
	soilPreisach state(-10.0,1.0);
	csvParser data;

	string inFile;

	cout << "Please enter the input filename: ";
	cin >> inFile;
	cout << "Thank you!" << el;


	data.parse(inFile.c_str());
	ofstream fout;
	inFile += ".out.csv";
	fout.open(inFile.c_str());
	fout.precision(8);
	cout.precision(8);
	fout << "#Point No." << sep << "Data Value (y)" << sep << "Found x-value" 
		<< sep << "Found y-value" << sep << "Preisach Derivative" << el;
	
	double acc = 0.5e-8;
	double ptNo, ptVal;
	double lstPt, lstVal;
	double left, right, mid;
	double lval, rval, mval;
	double step = 0.1;
	bool increasing = 1;

	clock_t start = clock();

//	Find initial point, take first value from input file
	ptNo = data(0,2);
	ptVal = data(0,1);
	right = -10.0;

	while( (rval = state.output(right)) < ptVal )
		right += step;
	lval = state.output(left = right - step);
//	Now we have lval which is below the desired value and rval which is above;
//	we will use bisection to obtain a better approximation.

	while( (right-left > acc) )
	{
		mval = state.output( mid = (left + right)/2.0 );
		if( (mval-ptVal)*(lval-ptVal) < 0 )
		{
			right = mid;
			rval = mval;
		}
		else
		{
			left = mid;
			lval = mval;
		}
	}
//	An interval is found containing the correct point, [left,right]. The point taken as output will be
//	(somewhat arbitrarily) the upper point if the input has increased to the current level, the lower
//	point if it has decreased. Since the initial step is assumed to be after an increase, we take
//	the right value here.

	state.add(lstPt = right);
	lstVal = state.output();

	fout << ptNo << sep << ptVal << sep << lstPt << sep << lstVal << sep << state.deri() << el;
//	cout << ptNo << sep << ptVal << sep << lstPt << sep << lstVal << el;

	int noRows = data.noOfRows();
//	Main loop through parsed data file
	for(int i = 1; i < noRows; i++)
	{
		ptNo = data(i,2);
		ptVal = data(i,1);
		if( ptVal > lstVal )	//	Increasing
		{
			increasing = 1;
			left = lstPt;
			lval = lstVal;
			while( (rval = state.output(right)) < ptVal )
				right += step;
		}
		else					//	Decreasing
		{
			increasing = 0;
			right = lstPt;
			rval = lstVal;
			while( (lval = state.output(left)) > ptVal )
				left -= step;
		}
//	We should now have our point bracketed by the points left and right, bisect to find it
		while( (right-left > acc) )
		{
			mval = state.output( mid = (left + right)/2.0 );
			if( (mval-ptVal)*(lval-ptVal) < 0 )
			{
				right = mid;
				rval = mval;
			}
			else
			{
				left = mid;
				lval = mval;
			}
		}
		
		if(increasing)
		{
			state.add(lstPt = right);
			lstVal = state.output();
		}
		else
		{
			state.add(lstPt = left);
			lstVal = state.output();
		}
		fout << ptNo << sep << ptVal << sep << lstPt << sep << lstVal << sep << state.deri() << el;
//		cout << ptNo << sep << ptVal << sep << lstPt << sep << lstVal << el;

	}	//	End of main for loop

	clock_t end = clock();
	cout << "Time taken for processing: " << (1.0*end-start)/CLOCKS_PER_SEC << el;
}

