#pragma once

const double hg = -1;
const double nm = 4;
const double mm = 0.5;
const double mpow = -1.0*(mm+1);
//const double qs = 0.5;
//const double q0 = 0.1;
const double c = 0.1;
//const double fn = 4.067362460553633e7;
const double A = -1.0 * (nm*mm) / ( (c-1)*hg*hg );
//(fn*mm*nm*qs)/((1.0-c)*hg*hg*(qs-q0));

double baseMeas(double alpha);
double integrand1(double alpha);
double integrand2(double alpha);
double integrand3(double alpha);
