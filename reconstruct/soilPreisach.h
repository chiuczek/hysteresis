#pragma once
#include "absPreisach.h"
#include "numInt.h"

class soilPreisach :
	public absPreisach
{
private:
	double triangle(const double x1, const double x2) const;
	double xLink(const double x1, const double x2) const;
	double yLink(const double y1, const double y2) const;
public:
	double measure(const double alph, const double bet) const;
	soilPreisach(const double &lb,const double &rb):absPreisach(lb,rb){};
	soilPreisach():absPreisach(){};
	soilPreisach(const double *pts, const int ptsl,
		const double lb,const double rb):absPreisach(pts,ptsl,lb,rb){};
	soilPreisach(const absPreisach &p):absPreisach(p){};
	virtual absPreisach* Clone() { return new soilPreisach(*this); }
};
