#include "gammas.h"

using namespace std;

double incGamP(const double a, const double x)
{
	double gamSer, gamCF, lnGam;

	if( x < 0. || a <= 0. )
	{
		cerr << "Invalid arguments to incomplete gamma function routine incGamP!\n";
		exit(1);
	}
	if( x < a + 1.0 )
	{
		serGam(gamSer,a,x,lnGam);
		return gamSer;
	}
	else
	{
		CFGam(gamCF,a,x,lnGam);
		return 1.0 - gamCF;
	}
}

void serGam(double &gSer, const double a, const double x, double &gLn)
{
	const int maxits = 100;
	const double eps = numeric_limits<double>::epsilon();
	double sum, del, ap;

	gLn = lnGam(a);
	if( x <= 0.0 )
	{
		if( x < 0.0 )
			exit(1);
		gSer = 0.0;
		return;
	}
	else
	{
		ap = a;
		del = sum = 1.0/a;
		for( int n = 0; n < maxits; n++ )
		{
			++ap;
			del *= x/ap;
			sum += del;
			if( fabs(del) < fabs(sum)*eps )
			{
				gSer = sum*exp(-x + a*log(x) - gLn);
				return;
			}
		}
		cerr << "Computation overflow in routine serGam!\n";
		exit(1);
	}
}

void CFGam(double &gCF, const double a, const double x, double &gLn)
{
	const int maxits = 100;
	const double eps = numeric_limits<double>::epsilon();
	const double fpmin = numeric_limits<double>::min()/eps;
	double an,b,c,d,del,h;
	int i;

	gLn = lnGam(a);
	b = x + 1.0 - a;
	c = 1.0/fpmin;
	d = 1.0/b;
	h = d;
	for(i = 1; i <= maxits; i++)
	{
		an = -i*(i-a);
		b += 2.0;
		d = an*d + b;
		if( fabs(d) < fpmin ) d = fpmin;
		c = b + an/c;
		if( fabs(c) < fpmin ) c = fpmin;
		d = 1.0/d;
		del = d*c;
		h *= del;
		if( fabs(del - 1.0) <= eps ) break;
	}
	if( i > maxits )
		cerr << "Maximum iterations exceeded in routine CFGam!\n";
	gCF = exp( -x + a*log(x) - gLn )*h;
}

double lnGam(const double xx)
{
	double x,y,tmp,ser;
	static const double cof[6] = {76.18009172947146,-86.50532032941677,
		24.01409824083091,-1.231739572450155,0.1208650973866179e-2,
		-0.5395239384953e-5};

	y = x = xx;
	tmp = x + 5.5;
	tmp -= (x + 0.5) * log(tmp);
	ser = 1.000000000190015;
	for( int j = 0; j < 6; j++ )
		ser += cof[j]/++y;
	return -tmp + log(2.5066282746310005*ser/x);
}

double errff(const double xx)
{
	return xx < 0.0 ? -incGamP(0.5,xx*xx) : incGamP(0.5,xx*xx);
}