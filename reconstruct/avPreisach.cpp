#include "avPreisach.h"
#include "utility.h"

using namespace std;

double tTemp;
double muTmp;
double s1Tmp;
double s2Tmp;

double avPreisach::output() const
{
	double S=-1;//-(rbound-lbound)*(rbound-lbound)/4; 
	if(arrl==0)
		return S;
	S += triangle(lbound,arr[0]);
	
	for(int i=0;i<arrl-1;i++)
	{
		S += triangle(arr[i],arr[i+1]);
	}
	return S;
}

void avPreisach::normalise()
{
	double res;

	tTemp = rbound;
	muTmp = centre;
	s1Tmp = dev1;
	s2Tmp = dev2;

	res = rombInt(triIntegrand,lbound,rbound);

	magTude = rbound;//2.0/res;

//	cout << magTude << endl;
}

///////////////////////////////////////////////////////////////////////////////////////
//	Triangle outputs the integral of the absPreisach density over the triangle
//defined by the points x1 and x2. The returned value is positive if x2 > x1,
//negative otherwise.
double avPreisach::triangle(const double x1, const double x2) const
{
	double s = Max(Min(x1,x2),lbound);
	double t = Min(Max(x1,x2),rbound);
	int sgn = (x1 < x2) ? 1 : -1;

	if ( s >= rbound || t <= lbound )
		return 0.;

	tTemp = t;

	muTmp = centre;
	s1Tmp = dev1;
	s2Tmp = dev2;
	
	return magTude*sgn*rombInt(triIntegrand,s,t);
}

//	Note: in xLink, we can assume that the y-value along which the
// absPreisach density is to be measured is equal to the larger of the two
// x-values passed. Similarly, for yLink, the x-value is the smaller of
// the two y-values passed.
double avPreisach::xLink(const double x1, const double x2) const
{
	double s = Min(x1,x2);
	double t = Max(x1,x2);
	double res = firstInt(t,t,-centre,dev1,dev2) - firstInt(t,s,-centre,dev1,dev2);
	return magTude*res;
}

double avPreisach::yLink(const double y1, const double y2) const
{
	double s = Min(y1,y2);
	double t = Max(y1,y2);
	double res = firstInt(s,t,centre,dev1,dev2) - firstInt(s,s,centre,dev1,dev2);
	return magTude*res;
}

double avPreisach::measure(const double alph, const double bet) const
{
	return magTude*gaussMeas(alph,bet,centre,dev1,dev2);
}

void avPreisach::getPars()
{
	cout << "Parameters missing for Preisach function!" << endl;
	cout << "Gaussian-type function requires three parameters:" << endl;
	cout << "Mean switching field: ";
	cin >> centre;
	cout << "Variance of switching field: ";
	cin >> dev1;
	cout << "Variance of relay width: ";
	cin >> dev2;
	cout << "Continuing with calculations...\n";
}

double gaussMeas(const double xx, const double yy, const double mu, const double sig1, const double sig2)
{
	double arg1 = xx + mu - yy;
	double arg2 = xx + yy;
	arg1 *= -arg1;
	arg2 *= -arg2;
	arg1 /= sig1;
	arg2 /= sig2;
	return exp(arg1 + arg2);
}

//		Indefinite integral of base Gaussian Preisach function.
//	For integral \int M(x,y) dy call as parameters suggest.
//	For integral \int M(x,y) dx call with y in place xx, x in yy and -mu.
double firstInt(const double xx, const double yy, const double mu, const double sig1, const double sig2)
{
	double res = 0.5;
	double tmp1, tmp2;
	tmp1 = sig1*sig2*PI;
	tmp2 = sig1+sig2;
	res *= sqrt(tmp1/tmp2);
	tmp1 = -(2*xx + mu)*(2*xx + mu)/tmp2;
	res *= exp(tmp1);
	tmp2 *= sig1*sig2;
	tmp1 = -sig1*(xx + mu - yy) + sig2*(xx + yy);
	tmp1 = tmp1/sqrt(tmp2);
	res *= errff(tmp1);
	return res;
}

double triIntegrand(const double xx)
{
	return firstInt(xx,tTemp,muTmp,s1Tmp,s2Tmp) - firstInt(xx,xx,muTmp,s1Tmp,s2Tmp);
}