//============================================================================
// Name        : enkfPreisach.cpp
// Author      : Hugh McNamara
// Version     :
// Copyright   : All rights reserved
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cmath>

#include "hyst.basic/absPreisach.h"
#include "hyst.basic/utility.h"

using namespace std;

int main()
{
  cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!

  unPreisach state1(-1,1);

  state1.add(0.0);

  for(int i = 0; i < 10; i++)
  {
    state1.add(sin(i*PI*2.0/9.0));
    cout << "Preisach state: " << state1.output() << endl;
  }

  return 0;
}
