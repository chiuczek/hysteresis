/************************************************************

	Header file declaring csvParser class for reading
	comma separated volume (CSV) formatted data files.
	Most of the goings on is hidden from the user, with
	parse and number being the main interface methods.
	
	See csvParser.cpp for implementation.

************************************************************/

#pragma once
#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdlib.h>
#include <string.h>

using namespace std;

class csvParser
{
public:
//	Public interface methods.
	//	Blank creator and destructor methods
	csvParser();
	~csvParser();
	//	Main data extraction method. Returns the
	//datum at position i,j in the file.
	double number(int i, int j);
	//	Number of rows of data in the file
	int noOfRows();
	//	Number of columns of data in the file
	int noOfColumns();
	//	Method which opens the input file and
	//parses the data. Needs only to be called
	//once, thereafter all the data can be
	//accessed using number and the other
	//interface methods.
	void parse(const char* fileName);
	double operator() (int i, int j) {return this->number(i,j);}
private:
//	Private storage and internal methods
	int fileRows;				//	Number of rows in the file
	int fileColumns;			//	Number of columns in the file
	int errorSwitch;			//	Has there been a file error
	void getInfo(int end);		//	Method to obtain information
								//on the data in the file prior to
								//loading it into memory. Info needed
								//includes fileRows and fileColumns etc.
	void fillNumbers(int end);	//	Fills the fileNumbers array with data
	ifstream inFile;			//	Input file stream
	double** fileNumbers;		//	Pointer to (dynamically allocated)
								//storage for the data in the file
	bool isDataParsed;
public:
	int fileError(void);		//	Public method to check for file errors
};
