#pragma once

const double hg = -0.8;
const double nm = 5;
const double mm = 0.8;
const double mpow = -1.0*(mm+1);
//const double qs = 0.27237;
//const double q0 = 0.008222030620874518;
const double c = 0.5;
//const double fn = 4.067362460553633e7;
const double A = -1.0 * (nm*mm) / ((c-1)*hg*hg);

double baseMeas(double alpha);
double integrand1(double alpha);
double integrand2(double alpha);
double integrand3(double alpha);