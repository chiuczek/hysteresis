#ifndef __chiu_avPreisach__
#define __chiu_avPreisach__
#pragma once

#include "absPreisach.h"
#include "gammas.h"
#include "numInt.h"

class avPreisach: public absPreisach
{
private:
	double triangle(const double x1, const double x2) const;
	double xLink(const double x1, const double x2) const;
	double yLink(const double y1, const double y2) const;

	void getPars();
	void normalise();

	double centre;
	double dev1;
	double dev2;
	double magTude;

public:
	double measure(const double alph, const double bet) const;
	avPreisach(const double bnds, const double cent, const double spr1, const double spr2)
		:absPreisach(-bnds,bnds),centre(cent),dev1(spr1),dev2(spr2){normalise();};
	avPreisach(const double &lb,const double &rb):absPreisach(lb,rb){getPars();normalise();};
	avPreisach():absPreisach(){getPars();normalise();};
	avPreisach(const double *pts, const int ptsl,
		const double lb,const double rb):absPreisach(pts,ptsl,lb,rb){getPars();normalise();};
	avPreisach(const absPreisach &p):absPreisach(p){getPars();normalise();};
	double output() const;
	virtual absPreisach* Clone() { return new avPreisach(*this); }
};

double gaussMeas(const double xx, const double yy, const double mu, const double sig1, const double sig2);
double firstInt(const double xx, const double yy, const double mu, const double sig1, const double sig2);
double triIntegrand(const double xx);
#endif