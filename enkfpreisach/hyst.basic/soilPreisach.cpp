#include "soilPreisach.h"
//#include "poulovassilis1.h"
#include "kerrysoil.h"

using namespace std;

double intEps = 1e-9;

double triPar;

//static ofstream intlog("intLog.txt");

///////////////////////////////////////////////////////////////////////////////////////
//	Triangle outputs the integral of the absPreisach density over the triangle
//defined by the points x1 and x2. The returned value is positive if x2 > x1,
//negative otherwise.
double soilPreisach::triangle(const double x1, const double x2) const
{
	if( fabs(x2-x1) < intEps )
		return intEps;
	int sgn = (x1 < x2) ? 1 : -1;
	double l = Min(x1,x2);
	double r = Max(x1,x2);
	double c1 = c*(l-rbound) + rbound;
	double c2 = (r-rbound)/c + rbound;
	double res;

//	intlog.precision(12);

	if( l >= rbound || r <= lbound )
		return 0;		//	If our triangle is outside the Preisach measure, 0
	if( fabs(l-lbound) < intEps && fabs(r-rbound) < intEps )
		return 1;		//	Shortcut for integral over the entire triangle

	if( r > c1 )
	{
//		intlog << "Triangle (2-part) integrated from " << l << " to " << r << endl;
		res = (c-1)*rombInt(integrand3,l,c2,intEps);
		triPar = r;
		res += rombInt(integrand2,c2,r,intEps);
	}
	else
	{
//		intlog << "Triangle (1-part) integrated from " << l << " to " << r << endl;
		triPar = r;
		res = rombInt(integrand2,l,r,intEps);
	}

	return sgn*res;
}

//	Note: in xLink, we can assume that the y-value along which the
// absPreisach density is to be measured is equal to the larger of the two
// x-values passed. Similarly, for yLink, the x-value is the smaller of
// the two y-values passed.
double soilPreisach::xLink(const double x1, const double x2) const
{
//	intlog.precision(12);

	if( fabs(x2-x1) < intEps )
		return intEps;
	double l,r;
	l = Max(-1,Min(x1,x2));
	r = Min(1,Max(x1,x2));
	double c2 = (r-1)/c + 1;

//	intlog << "X-link integrated from " << l << " to " << r << endl;

	if( l >= rbound || r <= lbound )
		return 0;		//	If our link is outside the Preisach measure, 0

	double res = rombInt(integrand1,Max(l,c2),r,intEps);
	return res;
}

double soilPreisach::yLink(const double x1, const double x2) const
{
//	intlog.precision(12);

	double l,r;
	l = Min(x1,x2);
	r = Max(x1,x2);
	double c1 = c*(l-1) + 1;

//	intlog << "Y-link integrated from " << l << " to " << r << endl;

	if( l >= rbound || r <= lbound )
		return 0;		//	If our link is outside the Preisach measure, 0

	double res = (Min(c1,r) - l)*baseMeas(l);
	return res;
}

double soilPreisach::measure(const double alph, const double bet) const
{
	return baseMeas(alph);
}

double baseMeas(double alpha)
{
	double x = (alpha-1.0)/hg;
	return A * pow(x,nm-2) * pow(1 + pow(x,nm),mpow);
}

double integrand1(double alpha)
{
	return baseMeas(alpha);
}

double integrand2(double alpha)
{
	return (triPar-alpha)*baseMeas(alpha);
}

double integrand3(double alpha)
{
	return (alpha-1.0)*baseMeas(alpha);
}