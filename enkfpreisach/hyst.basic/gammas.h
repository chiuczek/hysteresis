#ifndef __chiu_gammas__
#define __chiu_gammas__
#pragma once

#include <iostream>
#include <cmath>
#include <limits>
#include <cstdlib>

double incGamP(const double a, const double x);
void serGam(double &gSer, const double a, const double x, double &gLn);
void CFGam(double &gCF, const double a, const double x, double &gLn);
double lnGam(const double xx);
double errff(const double xx);

#endif
