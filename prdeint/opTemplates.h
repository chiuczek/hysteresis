#ifndef __chiu_fnOp_templates__
#define __chiu_fnOp_templates__

template<class Left,class Op,class Right>
struct fnOp1
{
	Left lfn;
	Right rfn;
	
	fnOp1(Left _l,Right _r):lfn(_l),rfn(_r){};
	
	double operator()(double a)
	{
		return Op::apply(lfn(a),rfn(a));
	}
	double operator()(double a, double b)
	{
		return Op::apply(lfn(a,b),rfn(a,b));
	}
};

template<class Op,class Right>
struct fnOp1<double,Op,Right>
{
	double xVal;
	Right rfn;
	
	fnOp1(double _x,Right _r):xVal(_x),rfn(_r){};
	
	double operator()(double a)
	{
		return Op::apply(xVal,rfn(a));
	}
	double operator()(double a, double b)
	{
		return Op::apply(xVal,rfn(a,b));
	}
};

template<class Outer,class Inner>
struct nestedFn
{
	Outer f;
	Inner g;
	
	nestedFn(Outer _f,Inner _g):f(_f),g(_g){};
	
	double operator()(double a)
	{
		return f( g(a) );
	};
	
	double operator()(double a,double b)
	{
		return f( g(a,b), b );
	};
};

template<class Left,class Right>
fnOp1<Left,Multiply,Right> operator*(Left a,Right b)
{
	return fnOp1<Left,Multiply,Right>(a,b);
};

template<class Left,class Right>
fnOp1<Left,Divide,Right> operator/(Left a,Right b)
{
	return fnOp1<Left,Divide,Right>(a,b);
};

template<class Left,class Right>
fnOp1<Left,Add,Right> operator+(Left a,Right b)
{
	return fnOp1<Left,Add,Right>(a,b);
};

template<class Left,class Right>
fnOp1<Left,Subtract,Right> operator-(Left a,Right b)
{
	return fnOp1<Left,Subtract,Right>(a,b);
};


#endif