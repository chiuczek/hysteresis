#include "absPreisach.h"

char absPreisach::separator = ',';

//int absPreisach::findarmax(const double *pts, const int ptsl,const int pos) const
int absPreisach::findarmax(const vector<double> pts, const int ptsl,const int pos) const
{
	double maxel=pts[pos];
	int cpos=pos;
	int ret=pos;
	while(++cpos<ptsl)
	{
		if(pts[cpos]>=maxel) 				
			ret=cpos;
	}
	return ret;
}

//int absPreisach::findarmin(const double* pts, const int ptsl, const int pos) const
int absPreisach::findarmin(const vector<double> pts, const int ptsl, const int pos) const
{
	if(ptsl<=pos) 			
	return -1;
	
	double minel=pts[pos];
	int ret=pos;
	int cpos=pos;
	while(++cpos<ptsl) 
	{
		if(pts[cpos]<=minel) 				
			ret=cpos;
	}
	return ret;
}

//bool absPreisach::reconstruct(const double* pts, const int ptsl)
bool absPreisach::reconstruct(const vector<double> pts, const int ptsl)
{
	int pos=-1;
	arrl=0;
	while(true)
	{
		pos=findarmax(pts,ptsl,pos+1);
		if(pos==-1) 
			break;
		arr[arrl++]=pts[pos];
		if(arrl>=max_arr_size) 
			throw range_error("absPreisach memory full");
		pos=findarmin(pts,ptsl, pos+1);
		if(pos==-1) 
			break;
		arr[arrl++]=pts[pos];
		if(arrl>=max_arr_size)
			throw range_error("absPreisach memory full");
	}
	return true;
}

absPreisach::absPreisach(const double &lb,const double &rb)
{
//	arr=new double[max_arr_size];
	arr = vector<double>(max_arr_size,0);
	lbound=lb;
	rbound=rb;
	arrl=1;
	arr[0]=lbound;
}
    
absPreisach::absPreisach()
{
//	arr=new double[max_arr_size];
	arr = vector<double>(max_arr_size,0);
	lbound=0;
	rbound=1;
	arrl=1;
	arr[0]=lbound;
}
    
absPreisach::absPreisach(const double *pts, const int ptsl, const double lb,const double rb)
{
//	arr=new double[max_arr_size];
	arr = vector<double>(max_arr_size,0);
	vector<double> temp(ptsl,0);
	for(int i = 0; i < ptsl; i++)
		temp[i] = pts[i];
	reconstruct(temp,ptsl);
	lbound=lb;
	rbound=rb;
}

absPreisach::absPreisach(const absPreisach &p)
{
//	arr=new double[max_arr_size];
	arr = vector<double>(max_arr_size,0);
	reconstruct(p.arr,p.arrl);
	lbound=p.lbound;
	rbound=p.rbound;
}

absPreisach::~absPreisach()
{
}
        
bool absPreisach::add(const double val)
{
	if(arrl>=max_arr_size)
		throw range_error("absPreisach memory full");
	if(val>rbound) 
	{
		arr[0]=rbound; //if |x| >=gamma then K=1 w(1)=sign(x)*gamma
		arrl=1;
		return true;
	}

	if(val<lbound)
	{
		arr[0]=lbound;
		arrl=1;
		return true;
	}
	
	if(arrl==0)
	{
		arr[arrl++]=val;
		return true;
	}
	
	if(val>arr[arrl-1]) //if x > w(Kold)
	{
		if( (arrl%2) == 1 ) 				
			arrl--;
		while(arrl>1)
		{
			if(arr[arrl-2]<=val) 
				arrl=arrl-2; 
			else 
				break;
		}
            
		arr[arrl++]=val;
		return true;
	}
	
	if(val<arr[arrl-1])
	{
		if( (arrl% 2)==0 )
			arrl--;
		
		while(arrl>2)
			if(arr[arrl-2]>=val)
				arrl=arrl-2; 
			else
				break;
		
		arr[arrl++]=val;
		return true;
	}
	return true;
}
    
int absPreisach::wouldbepos(const double val) const
{
	int arrl = this->arrl;
	
	if(val>rbound) 
	{
		return 0;
	}
	
	if(val<lbound)
	{
		return 0;
	}
	
	if(arrl==0)
	{
		return 0;
	}
	
	if(val>=arr[arrl-1]) 
	{
		if( (arrl% 2)==1 ) 				
			arrl--;
		while(arrl>1)
			if(arr[arrl-2]<=val)
				arrl=arrl-2;
			else 
				break;
		
		return arrl;
	}
	
	if(val<=arr[arrl-1]) 
	{
		if( (arrl% 2)==0 )
			arrl--;
		
		while(arrl>2)
			if(arr[arrl-2]>=val) 
				arrl=arrl-2; 
			else 
				break;
		
		return arrl;
	}
	return arrl;
}    
ostream& absPreisach::Print(ostream &out) const
{
	ostringstream temp;
	temp.precision(14);
	for(int i=0;i<arrl;i++)
	{
		temp << arr[i];
		temp << separator;
	}
	temp << separator << arrl;
	return out << temp.str();
}

double absPreisach::lastPoint() const 
{
	return arr[arrl-1];
}

void absPreisach::changeBounds(const double lb, const double rb)
{
	lbound=lb;
	rbound=rb;
	vector<double> tmp(1,lb);
//	double tmp[1]={lb};
	reconstruct(tmp,1);
}

double absPreisach::getMax() const
{
	return rbound;
}

double absPreisach::getMin() const
{
	return lbound;
}

bool absPreisach::isIncreasing() const
{
	return arrl%2 ? true : false;
}

double absPreisach::output() const
{
	double S=0;//-(rbound-lbound)*(rbound-lbound)/4; 
	if(arrl==0)
		return S;
	S += triangle(lbound,arr[0]);
	
	for(int i=0;i<arrl-1;i++)
	{
		S += triangle(arr[i],arr[i+1]);
	}
	return S;
}

    
double absPreisach::output(const double val, const bool ev)
{
	if(ev)
	{
		add(val);
		return output();
	}
	else
		return wouldbesum(val, wouldbepos(val));
}


double absPreisach::wouldbesum(const double v, const int pos) const
{
	double val=v;
	if(val>rbound)
		val=rbound;
	if(val<lbound) 
		val=lbound;
	
	double S=0;
	if(arrl==0) 
		return S;
	if(pos == 0 ) 
		return S + triangle(lbound,val);
	else 
		S += triangle(lbound,arr[0]);
	
	for(int i=0;i<pos-1;i++)
	{
		S += triangle(arr[i],arr[i+1]);
	}
	S += triangle(arr[pos-1],val);
	return S;
}

double absPreisach::deri() const
{
	if(arrl == 0)
		return 0;
	else if(arrl == 1)
		if(arr[0] == lbound)
			return 0;
		else
			return xLink(arr[0],lbound);//arr[0]-lbound;
	else if(arrl % 2 == 0)
		return yLink(arr[arrl-2],arr[arrl-1]);
	else
		return xLink(arr[arrl-1],arr[arrl-2]);
}

double absPreisach::deri(const double val, const bool ev)
{
	int pos1;
	
	if(ev)
	{
		add(val);
		pos1 = arrl;
	}
	else
		pos1 = wouldbepos(val)+1;
	
	if(pos1 == 0)
		return 0;
	else if(pos1 == 1)
		return xLink(val,lbound);
	else if(pos1 % 2 == 0)
		return yLink(arr[pos1-2],val);
	else
		return xLink(val,arr[pos1-2]);
}

///////////////////////////////////////////////////////////////////////////////////////
//	Triangle outputs the integral of the absPreisach density over the triangle
//defined by the points x1 and x2. The returned value is positive if x2 > x1,
//negative otherwise.
double unPreisach::triangle(const double x1, const double x2) const
{
	int sgn = (x1 < x2) ? 1 : -1;
	return sgn*(x1-x2)*(x1-x2)/2;
}

//	Note: in xLink, we can assume that the y-value along which the
// absPreisach density is to be measured is equal to the larger of the two
// x-values passed. Similarly, for yLink, the x-value is the smaller of
// the two y-values passed.
double unPreisach::xLink(const double x1, const double x2) const
{
	return (x1 > x2) ? (x1 - x2) : (x2 - x1);
}

double unPreisach::yLink(const double y1, const double y2) const
{
	return (y1 > y2) ? (y1 - y2) : (y2 - y1);
}

double unPreisach::measure(const double alph, const double bet) const
{
	return 1;
}