#include "kalpreis.h"

double kalPreis::triangle(const double x1, const double x2) const
{
	double comp,res,s,t;
	int sgn;

	if(x1 < x2)
	{
		sgn = 1;
		s = x1;
		t = x2;
	}
	else
	{
		sgn = -1;
		s = x2;
		t = x1;
	}

	comp = (hystPar == 0) ? t : (s - lbound)/hystPar + lbound;

	if(s >= t)
		return 0;

	if(t >= rbound)
		t = rbound;

	if(s <= lbound)
		res = atan(t) - atan(lbound);

	else if( t <= comp )
	{
		res = (s - lbound)*log( ( (intPow(s,2)+1)*intPow(t-lbound,2) )/( (intPow(t,2)+1)*intPow(s-lbound,2) ) );
		res += 2*(s*lbound + 1)*(atan(s) - atan(t));
		res /= 2.*(hystPar - 1.)*(1 + intPow(lbound,2));
	}
	else
	{
		res = (s - lbound)*log( (intPow(s,2)+1) / (intPow(hystPar,2) + intPow(s+hystPar*lbound-lbound,2)) );
		res += 2*(s*lbound + 1)*(atan(s) - atan(lbound + (s-lbound)/hystPar));
		res /= 2.*(hystPar - 1.)*(1 + intPow(lbound,2));
		res += atan(t) - atan(lbound + (s-lbound)/hystPar);
	}

	return sgn*res;
}

double kalPreis::xLink(const double x1, const double x2) const
{
	double a,b;
	int sgn;
	if(x2 > x1)
	{
		a = x1;
		b = x2;
		sgn = -1;
	}
	else
	{
		b = x1;
		a = x2;
		sgn = 1;
	}

	if(b <= lbound + eps)
		return 0;

	if(a < hystPar*(b - lbound) + lbound)
		return sgn*1./(1+intPow(b,2));
	else
		return sgn*(a - b)/((1 + intPow(b,2))*(hystPar-1.)*(b - lbound));
}

double kalPreis::yLink(const double y1, const double y2) const
{
	double a,b,res;
	int sgn;
	if(y2 > y1)
	{
		a = y1;
		b = y2;
		sgn = 1;
	}
	else
	{
		b = y1;
		a = y2;
		sgn = -1;
	}

	if(a <= lbound + eps)
		return 0;

	res = log( ( (1+intPow(a,2))*intPow(b-lbound,2) ) / ( (1+intPow(b,2))*intPow(a-lbound,2) ) );
	res += 2*lbound*( atan(a) - atan(b) );
	res /= 2*(hystPar-1)*(1+intPow(lbound,2));

	return sgn*res;
}

double kalPreis::measure(const double alph, const double bet) const
{
	if( (alph > bet) || (alph < lbound) || (bet > rbound) )
		return 0;
	if( bet > (alph - lbound)/hystPar + lbound )
		return 0;
	else
	{
		double denom = (1 - hystPar)*(bet - lbound)*(1 + intPow(bet,2));
		return 1./denom;
	}
}

double kalPreis::scalOut() const
{
	return output();
}

double kalPreis::scalOut(const double val,const bool ev)
{
	return output(val,ev);
}
