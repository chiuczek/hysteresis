#ifndef __chiu_infunc__
#define __chiu_infunc__
#pragma once
#include "utility.h"

class inFunc
{
private:
	pfn2 function;
	pfn2 deriv;
public:
	inFunc(pfn2 fn, pfn2 dfn)
		:function(fn),deriv(dfn){};
	
	double operator()(double t,double x = 0)
	{
		return function(t,x);
	}
	double ddt(double t,double x = 0)
	{
		return deriv(t,x);
	}
};

class disconFunc: public inFunc
{
private:
	pfn2 getNextZero;
public:
	disconFunc(pfn2 fn,pfn2 dfn,pfn2 zerfn)
		:getNextZero(zerfn),inFunc(fn,dfn){};
	
	double next0(double t,double x)
	{
		return getNextZero(t,x);
	}
};

inline double F(const double t,const double x)
{
	return 0.1*(cos(PI*t) + cos(PI*rt2*t));
};

inline double dFdt(const double t,const double x)
{
	return -0.1*PI*(sin(PI*t) + rt2*sin(PI*rt2*t));
};


#endif