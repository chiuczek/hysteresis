#pragma once
#include "PrDEsolver.h"

char PrDEsolver::separator = ',';
double PrDEsolver::tauLinStep = 1e-5;
double PrDEsolver::tauMax = 0.0001;
double PrDEsolver::tauMin = 1e-8;

void PrDEsolver::rkInt(double h)
{
	static const double a2 = 0.2,a3 = 0.3,a4 = 0.6,a5 = 1.0,a6 = 0.875,
		b21 = 0.2,b31 = 3./40.,b32 = 9./40.,b41 = 0.3,b42 = -0.9,b43 = 1.2,
		b51 = -11./54.,b52 = 2.5,b53 = -70./27.,b54 = 35./27.,
		b61 = 1631./55296.,b62 = 175./512.,b63 = 575./13824.,
		b64 = 44275./110592.,b65 = 253./4096.,
		c1 = 37./378.,c3 = 250./621.,c4 = 125./594.,c6 = 512./1771.,
		dc1 = c1 -2825./27648.,dc3 = c3 - 18575./48384.,
		dc4 = c4 - 13525./55296.,dc5 = -277./14336.,dc6 = c6 - 0.25;
	
	double ak2,ak3,ak4,ak5,ak6,ytmp;
	
	ytmp = y + h*b21*dydt;
	ak2 = right(t + a2*h, ytmp);
	ytmp = y + h*( b31*dydt + b32*ak2 );
	ak3 = right(t + a3*h, ytmp);
	ytmp = y + h*( b41*dydt + b42*ak2 + b43*ak3 );
	ak4 = right(t + a4*h, ytmp);
	ytmp = y + h*( b51*dydt + b52*ak2 + b53*ak3 + b54*ak4 );
	ak5 = right(t + a5*h, ytmp);
	ytmp = y + h*( b61*dydt + b62*ak2 + b63*ak3 + b64*ak4 + b65*ak5 );
	ak6 = right(t + a6*h, ytmp);
	
	ynew = y + h*( c1*dydt + c3*ak3 + c4*ak4 + c6*ak6 );
	yerr = h*( dc1*dydt + dc3*ak3 + dc4*ak4 + dc5*ak5 + dc6*ak6 );
};


inline void PrDEsolver::setDataSeparator(const char c)
{
	separator = c;
	state.setDataSeparator(c);
};

void PrDEsolver::linStep()
{
	ynew = y + linKappa()*tauLinStep;

	cout << "Linear step from t = " << t << " to t = " << t+tauLinStep 
		<< ", y = " << y << " to y = " << ynew << endl;

	ylast = y;
	y = ynew;
	t = t + tauLinStep;
	tauLast = tauLinStep;
	tauNext = tauOrg;
	yerr = -1;
};

void PrDEsolver::preWipeStep()
{
	int bisectSign = 1, n = 1;
	double dt = 0;
	double yold = y;		//	When deciding to use a preWipeStep, rkInt is called.
							//	Thus, y lies before yJump, ynew after it.
							//	We use bisection to get closer to yJump.
	
	while( fabs(ynew-yJump) > eps )
	{
		n *= 2;
		dt += bisectSign*( tauLast/n );
		rkInt(dt);
		if( (yold - yJump)*(ynew - yJump) < 0 )
			bisectSign = -1;
		else
			bisectSign = +1;
	}

	cout << "preWipe step from t = " << t << " to t = " << t+dt 
		<< ", y = " << y << " to y = " << ynew << endl;

	tauLast = dt;
	tauNext = tauOrg;	//	Reset step size to original.
	ylast = y;
	y = yJump;
	t += dt;
	state.add(y);
	yJump = state.nextWipe();
};

void PrDEsolver::preLinStep()
{
	int bisectSign = 1, n = 1;
	double dt = 0;
//	double yold = ynew;						//	Again, y and ynew are now on opposite sides of the 
	double fold = right();					//	zero in f. We will use bisection to find the exact
	double fnew = right(t + tauLast,ynew);	//	point, and the next step will be a linear approx.
	
	do
	{
		n *= 2;
		dt += bisectSign*(tauLast/n);
		rkInt(dt);
		fnew = f(t+dt,ynew);
		if( fnew*fold < 0 )
			bisectSign = -1;
		else
			bisectSign = +1;
	}
	while( fabs(fnew) > 0.001*eps );


	cout << "preLin step from t = " << t << " to t = " << t+dt 
		<< ", y = " << y << " to y = " << ynew << endl;

	tauLast = dt;
	tauNext = tauLinStep;
	ylast = y;
	y = ynew;
	state.add(y);
	t += dt;
	dir = yDirection();
};

void PrDEsolver::normStep()		//	A normal step for PrDEsolver.
								//	Note: this is the same code as in the rkStep
								//	function in the base class, but it doesn't alter
								//	y or t, instead info on the results will be found
								//	in ynew and in tauLast.
{
	double err,h,htmp,tnew;
	static const double SAFFACT = 0.9, GROWPOW = -0.2,
		SHRNKPOW = -0.25, ERRCON = 1.89e-4;

	h = tauNext;

	dydt = right();		//	NOTE: right is a different function
							//	to the rhs in rkSolver.
//	yscal = fabs(y) + fabs(h*dydt);
	while(1)
	{
		rkInt(h);
		err = 100*fabs(yerr/yscal);		//	We want the normal integration steps to be
		err /= eps;						//	very accurate, so use 100 times more precision.

		if(err <= 1.0) break;

		htmp = SAFFACT*h*pow(err,SHRNKPOW);

		h = ( h >= 0.0 ? Max(htmp,0.1*h) : Min(htmp,0.1*h) );

//		if( h < tauMin )
//		{
//			cout << "Step too small, exiting normstep()" << endl;
//			break;
//		}
		
		tnew = t + h;

		if(tnew == t)
		{
			cerr<<"Error, underflow in RK solver - stepsize too small!"<<endl;
			throw(0);
		}
	};

	if(err > ERRCON)
		tauNext = SAFFACT*h*pow(err,GROWPOW);
	else
		tauNext = 5*h;

	if(tauNext < tauMin)
		tauNext = tauMin;
	if(tauNext > tauMax)
		tauNext = tauMax;

	tauLast = h;

//	t += (tauLast = h);
//	y = ynew;
};

void PrDEsolver::step()
{
	static ofstream stepOut("steps.csv");

	if( nextStep != linear && f(t,y)*f(t + tauLast,y) < 0 )
	{
		rkInt(tauLast);
		preLinStep();
		nextStep = linear;
		lastStep = toLin;
	}
	else if( nextStep != linear )
	{
		normStep();		//	normStep executes a normal RK integration step,
//		dydt = right();	//	with adaptive step control, without altering y or t.
//		rkInt(tauOrg);	//	The resulting point is in ynew, the timestep used in tauLast.
		//	Normal step executed, now we must check if we need a preWipe or a preLin step.
		//	Check wiping first:
		if( (y - yJump)*(ynew - yJump) < 0 )
		{
			preWipeStep();
			nextStep = normal;
			lastStep = toWipe;
		}
		else if( fabs(f(t,y)) < 1.e-8 )
		{
			rkInt(tauOrg);
			preLinStep();
			nextStep = linear;
			lastStep = toLin;
		}
		else
		{
			ylast = y;
			y = ynew;
			state.add(y);
			t += tauLast;
			nextStep = normal;
			lastStep = normal;
			yJump = state.nextWipe();
		}
	}
	else
	{
		linStep();
		state.add(y);
		dir = yDirection();
		yJump = state.nextWipe();
		nextStep = normal;
		lastStep = linear;
	}
	if( lastStep != normal )
	{
		switch(lastStep)
		{
		case linear:
			stepOut << "Linear step taken" << separator;
			break;
		case toWipe:
			stepOut << "Step to wiping out taken" << separator;
			break;
		case toLin:
			stepOut << "Step to turning point taken" << separator;
			break;
		}
		stepOut << *this << endl;

	}
};

int PrDEsolver::yDirection()
{
	if( !(fabs(f(t,y)) < eps) )
	{
		return sgn(f(t,y));
	}
	else if( !(fabs(dfdt(t,y)) < eps) )
	{
		return sgn(dfdt(t,y));
	}
	else
	{
		cerr << "Solution inderterminate!" << endl;
		cerr << "Both f and df/dt vanish at t = "
			<< t << ", y = " << y << endl;
		throw(0);
	}
};

double PrDEsolver::right(double _t, double _y)
{
	return f(_t,_y)/state.deri(_y);
};
double PrDEsolver::right()
{
	return f(t,y)/state.deri();
}

double PrDEsolver::linKappa()
{
	double disc,res;
	double a = linCoeffA();
	double b = linCoeffB();
	if( dir > 0 && b > eps )
	{
		disc = sqrt( intPow(a,2) + 4*b );
		res = (a + disc)/2.;
	}
	else if( dir < 0 && b < -eps )
	{
		disc = sqrt( intPow(a,2) - 4*b );
		res = (-a - disc)/2.;
	}
	else
	{
		cerr << "Error in linear step mechanism!" << endl;
		cerr << "f, dfdt and Q all vanish at:" << endl;
		cerr << "y = " << y << endl;
		cerr << "t = " << t << endl;
		throw(0);
	}
	return res;
};

double PrDEsolver::linCoeffA()
{
	double res = dfdy(t,y);
	res /= state.measure(y,y);
	return res;
};

double PrDEsolver::linCoeffB()
{
	double res = dfdt(t,y);
	res /= state.measure(y,y);
	return res;
};