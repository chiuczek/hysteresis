#ifndef __chiu_abs_preis__
#define __chiu_abs_preis__

#include <iostream>
#include <fstream>
#include <stdexcept>
#include <sstream>
#include <limits>
#include <vector>

using namespace std;

const double eps = 1000*numeric_limits<double>::epsilon();
const double epsUp = 1 + eps;
const double epsDn = 1 - eps;

//////////////////////////////////////////////////////////////////////////////
//					Abstract Preisach class									//
//		Class implementing generic interface for any Preisach nonlinearity.	//
//	Includes declarations of pure virtual functions for the specification	//
//	of the use of a particular Preisach density.							//
//		The interface for adding input values, evaluating the current		//
//	output or "derivative" of the Preisach nonlinearity, evaluating the		//
//	same vaules if a given value were input, is common for any derived		//
//	class. The private functions triangle, xLink and yLink are all that is	//
//	required to specify a particular Preisach nonlinearity.					//
//		These functions return the values of the integral of the Preisach	//
//	density used over 														//
//	(a)	a triangle with vertices (x1,x1),(x2,x2),(x1,x2) (following the 	//
//		convention triangle(b,a) = -triangle(a,b)),							//
//	(b)	a segment of the line y = max(x1,x2) between the end points			//
//		min(x1,x2) and max(x1,x2).											//
//	(c) a segment of the line x = min(x1,x2) between the end points			//
//		min(y1,y2) and max(y1,y2).											//
//	The integrals can be in closed form, or call a numerical integration	//
//	scheme, all that is required is to override the pure virtual functions	//
//	(a)	double triangle(const double x1, const double x2) const				//
//	(b)	double xLink(const double x1, const double x2) const				//
//	(c)	double yLink(const double y1, const double y2) const				//
//	respectively.															//
//		Also included is a sample derived class unPreisach, which uses the	//
//	uniform density (triangle integration given by area, x or yLink given	//
//	by length).																//
//////////////////////////////////////////////////////////////////////////////

class absPreisach 
{
protected:
	//		Maximum length of array of shocks. If your input is very variable over
	//	many scales, this may not be enough, but it should be sufficient for most uses.
	static const int max_arr_size=1000;

	double lbound;	//		Preisach measure is only non-zero in the triangle with
	double rbound;	//	vertices (lbound,lbound),(lbound,rbound),(rbound,rbound).
//	double *arr;	//		arr points to the arrl shock values specifying the state
	vector<double> arr;
	int arrl;		//	of the Preisach measure.

	static char separator;

//		Internal functions used in restructuring an unordered array of input values.
//	int findarmax(const double *pts, const int ptsl, const int pos) const;
	int findarmax(const vector<double> pts, const int ptsl, const int pos) const;
//	int findarmin(const double* pts, const int ptsl, const int pos) const;
	int findarmin(const vector<double> pts, const int ptsl, const int pos) const;

//		Returns the value arrl would take IF the value val were input.
	int wouldbepos(const double val) const;

	ostream &Print(ostream &out = std::cout) const;

//		Pure virtual functions, described above. Specialisation of these
//	serves as the means of defining the Preisach measure used in derived
//	classes.
	virtual double triangle(const double x1, const double x2) const = 0;
	virtual double xLink(const double x1, const double x2) const = 0;
	virtual double yLink(const double y1, const double y2) const = 0;
public:
	virtual double measure(const double alph, const double bet) const = 0;

//		Reconstruct an array of input values of length pts1. This is equivalent
//	to looping over pts, calling add(pts[i]) at each step.
//	bool reconstruct(const double* pts, const int ptsl);
	bool reconstruct(const vector<double> pts, const int ptsl);

//		Most useful constructor if you are going to be adding points step-by-step
	absPreisach(const double &lb,const double &rb);
//		Default constructor
	absPreisach();
//		Constructor using an array of inputs - uses reconstruct to fill Preisach state
	absPreisach(const double *pts, const int ptsl, const double lb,const double rb);
//		Copy constructor
	absPreisach(const absPreisach &p);

//		Virtual destructor - if you allocate more memory (e.g. using new) in the
//	constructor of your derived class and deallocate in its destructor, the memory
//	pointed to be arr will still be deallocated.	
	virtual ~absPreisach();

//		Common interface functions.
//	Evolves internal state using input val
	bool add(const double val);
//	Returns the last added point - current input value
	double lastPoint() const;
//	Returns the value of arrl
	inline int size() const
	{
		return arrl;
	};
	inline double operator[](const int i)
	{
		return arr[i];
	};
//	Restructure the Preisach history with new bounds.
	void changeBounds(const double lb, const double rb);
//	Returns the right bound (for getMax), or the left bound (for getMin).
	double getMax() const;
	double getMin() const;
//	Returns true if input has been increasing (i.e. the last Preisach link is
//	horizontal), false if decreasing (vertical).
	bool isIncreasing() const;
//	Returns the output of the Preisach nonlinearity in the current state.
//	Note that nothing need be done to this function to specify a particular
//	measure, it uses the routine triangle to calculate the output given
//	the state, so redefine triangle to change density.
//	The second version returns what the output would be if val were first
//	added. This is useful because adding a value is likely destructive of
//	history information.
//		In particular, preis.output(val); returns the same value
//	as the combination pries.add(val); preis.output(); but no changes are
//	made to the internal Preisach state.
	double output() const;
	double output(const double val, const bool ev = 0);
//	Returns the output if v were added AFTER WIPING OUT a number of past
//	shocks (the number of shocks wiped out is arrl-pos-1)
	double wouldbesum(const double v, const int pos) const;
//	Returns the Preisach "derivative", given by the integral of the measure
//	over the last link in the characteristic function. Uses xLink if input
//	has been increasing, yLink if decreasing.
//	Again, second version returns the result IF val were input.
	double deri() const;
	double deri(const double val, const bool ev = 0);
//	Returns the next threshold for a wiping out of an internal
//	shock.
	inline double nextWipe()
	{
		if(arrl < 3)
			return arrl%2 ? -1e30 : 1e30;
		else
			return arr[arrl-3];
	}
	
	inline void setDataSeparator(const char c)
	{
		separator = c;
	}
	
//	Friend function overloads the output stream operator.
//	Tested for file output streams and cout.
//	I think this would work for string streams, but I haven't tested it.
	inline friend ostream &operator<<(std::ostream &out, const absPreisach &abp)
	{
		return abp.Print(out);
	};
};



class unPreisach: public absPreisach
{
private:
	double triangle(const double x1, const double x2) const;
	double xLink(const double x1, const double x2) const;
	double yLink(const double y1, const double y2) const;
public:
	double measure(const double alph, const double bet) const;
	unPreisach(const double &lb,const double &rb):absPreisach(lb,rb){};
	unPreisach():absPreisach(){};
	unPreisach(const double *pts, const int ptsl,
		const double lb,const double rb):absPreisach(pts,ptsl,lb,rb){};
	unPreisach(const absPreisach &p):absPreisach(p){};
};

#endif