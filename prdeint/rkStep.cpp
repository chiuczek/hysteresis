#include "rkStep.h"

using namespace std;

double rkSolver::tauMax = 0.0001;
double rkSolver::tauMin = 1e-8;

void rkSolver::rkInt(double h)
{
	static const double a2 = 0.2,a3 = 0.3,a4 = 0.6,a5 = 1.0,a6 = 0.875,
		b21 = 0.2,b31 = 3./40.,b32 = 9./40.,b41 = 0.3,b42 = -0.9,b43 = 1.2,
		b51 = -11./54.,b52 = 2.5,b53 = -70./27.,b54 = 35./27.,
		b61 = 1631./55296.,b62 = 175./512.,b63 = 575./13824.,
		b64 = 44275./110592.,b65 = 253./4096.,
		c1 = 37./378.,c3 = 250./621.,c4 = 125./594.,c6 = 512./1771.,
		dc1 = c1 -2825./27648.,dc3 = c3 - 18575./48384.,
		dc4 = c4 - 13525./55296.,dc5 = -277./14336.,dc6 = c6 - 0.25;
	
	double ak2,ak3,ak4,ak5,ak6,ytmp;
	
	ytmp = y + h*b21*dydt;
	ak2 = rhs(t + a2*h, ytmp);
	ytmp = y + h*( b31*dydt + b32*ak2 );
	ak3 = rhs(t + a3*h, ytmp);
	ytmp = y + h*( b41*dydt + b42*ak2 + b43*ak3 );
	ak4 = rhs(t + a4*h, ytmp);
	ytmp = y + h*( b51*dydt + b52*ak2 + b53*ak3 + b54*ak4 );
	ak5 = rhs(t + a5*h, ytmp);
	ytmp = y + h*( b61*dydt + b62*ak2 + b63*ak3 + b64*ak4 + b65*ak5 );
	ak6 = rhs(t + a6*h, ytmp);
	
	ynew = y + h*( c1*dydt + c3*ak3 + c4*ak4 + c6*ak6 );
	yerr = h*( dc1*dydt + dc3*ak3 + dc4*ak4 + dc5*ak5 + dc6*ak6 );
};

void rkSolver::rkStep()
{
	double err,h,htmp,tnew;
	static const double SAFFACT = 0.9, GROWPOW = -0.2,
		SHRNKPOW = -0.25, ERRCON = 1.89e-4;

	h = tauNext;

	dydt = rhs(t,y);
	yscal = fabs(y) + fabs(h*dydt);
	while(1)
	{
		rkInt(h);
		err = fabs(yerr/yscal);
		err /= eps;

		if(err <= 1.0) break;

		htmp = SAFFACT*h*pow(err,SHRNKPOW);

		h = ( h >= 0.0 ? Max(htmp,0.1*h) : Min(htmp,0.1*h) );
		
		tnew = t + h;

		if(tnew == t)
		{
			cerr<<"Error, underflow in RK solver - stepsize too small!"<<endl;
			throw(0);
		}
	};

	if(err > ERRCON)
		tauNext = SAFFACT*h*pow(err,GROWPOW);
	else
		tauNext = 5*h;

	if(tauNext < tauMin)
		tauNext = tauMin;
	if(tauNext > tauMax)
		tauNext = tauMax;

	tauLast = h;

//	t += (tauLast = h);
//	y = ynew;
};

void rkSolver::step()
{
	rkStep();
	t += tauLast;
	y = ynew;
};