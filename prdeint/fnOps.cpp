#pragma once
#include "fnOps.h"

double retZero(double t)
{
	return 0;
};

double oneFn(double t)
{
	return t;
};
fn1d One(oneFn);
fn1d nullFn1d(retZero);

double retZero(double a,double b)
{
	return 0;
};

fn2d nullFn2d(retZero);

template<class Inner,class Outer>
outerFn<Inner,Outer> outer(Inner left, Outer right)
{
	return outerFn<Inner,Outer>(left,right);
};

template<class Right>
fnOp1<fn1d,Subtract,Right> operator-(Right a)
{
	return fnOp1<fn1d,Subtract,Right>(nullFn1d,a);
};

fn1d Sin(sin);
fn1d Cos(cos);
fn1d Tan(tan);
fn1d Log(log);
fn1d Exp(exp);
fn1d Atan(atan);
fn1d Acos(acos);
fn1d Asin(asin);
fn1d Cosh(cosh);
fn1d Sinh(sinh);
fn1d Tanh(tanh);