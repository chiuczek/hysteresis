#ifndef __chiu_kal_preis__
#define __chiu_kal_preis__

#include "utility.h"
#include "absPreisach.h"
#include <cmath>

//		Derived Preisach class using measure used in hysteretic Kaldor model.
//	Also includes simpler interface functions.
class kalPreis: public absPreisach
{
protected:
	double hystPar;
	double triangle(const double x1, const double x2) const;
	double xLink(const double x1, const double x2) const;
	double yLink(const double y1, const double y2) const;
	double measure(const double alph, const double bet) const;
	
	double curIn;
	double curOut;
	bool outUpDated;
public:
	kalPreis(const double &lb,const double &rb):absPreisach(lb,rb),hystPar(0){};
	
	kalPreis():absPreisach(),hystPar(0){};
	
	kalPreis(const double *pts, const int ptsl,
		const double lb,const double rb):absPreisach(pts,ptsl,lb,rb),hystPar(0){};
		
	kalPreis(const absPreisach &p):absPreisach(p),hystPar(0){};
	
	kalPreis(const double c, const double left, const double init)
		:absPreisach(left,200),hystPar(c),curIn(0),outUpDated(0),curOut(0){ add(init); };
		
	kalPreis(const double c, const double left, const double right, const double init)
		:absPreisach(left,right),hystPar(c),curIn(0),outUpDated(0),curOut(0){ add(init); };
		
	double scalOut() const;
	double scalOut(const double val,const bool ev = 0);

	//	Overloaded function operator - for use in the Kaldor model.
	//	Adds in arctan behaviour outside bounds of Preisach.
	double operator()(const double u,const int evolve = 1)
	{
		if(outUpDated && fabs(1-curIn/u) <= eps)
			return(curOut);
		if(evolve)
		{
			add(u);
			curIn = u;
			outUpDated = 1;
			if(u < lbound || u > rbound)
				curOut = atan(u);
			else
				curOut = output() + atan(lbound);
			return curOut;
		}
		else
		{
			if(u < lbound || u > rbound)
				return atan(u);
			else return atan(lbound) + output(u);
		}
	}
};

#endif