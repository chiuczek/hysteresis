#include "PrDEsolver.h"
#include "kalpreis.h"
#include <ctime>
#include <cstdlib>
#include <sstream>

double period;
double k = 0.5;
double a = 0.5;

inline double F(const double t,const double y)
{
	return -k*(y - a*(cos(PI*t/period) + cos(PI*rt2*t/period)));
//	return 0.3*(cos(PI*t/period) + cos(PI*rt2*t/period));
};

inline double dFdt(const double t,const double y)
{
	return -k*a*PI/period*(sin(PI*t/period) + rt2*sin(PI*rt2*t/period));
//	return -0.3*PI/period*(sin(PI*t/period) + rt2*sin(PI*rt2*t/period));
};

inline double dFdy(const double t,const double y)
{
	return -k;
//	return 0;
};

int main(int argc, char* argv[])
{
	double maxT = 10;

	if(argc >= 2)
	{
		period = atof(argv[1]);
	}
	else period = 3.45;

	double c;
	if(argc == 3)
	{
		c = atof(argv[2]);
	}
	else c = 0.5;

	kalPreis testState(c,-1.,1.,0);
//	unPreisach testState(-1,1);
//	testState.add(0);
	PrDEsolver solver(&testState,0,0,1e-5,1e-5,F,dFdt,dFdy);
	ofstream tout;
	ostringstream fileName;

	fileName << "PrDE.T" << period
		<< ".csv";
	tout.open(fileName.str().c_str());
	cout << "Results stored in " << fileName.str().c_str() << endl;

	clock_t start,finish;

	double tInside = 0;

	tout << "#,t,f,dfdt,rhs,,out,Pr,PrDer" << endl;

	int i = 0;

	start = clock();
	while( tInside < maxT )
	{
		solver.step();
		tInside = solver.tOut();
		i++;
		if( i % 10 == 0 || !solver.isStepNorm() )
		{
			tout <<  solver << endl;
		}
	}
	finish = clock();
	cout << endl << "Reached endpoint in " << i << " iterations." << endl;
	cout << "Time taken = " << (finish - start)/(1.*CLOCKS_PER_SEC) << endl;
}
