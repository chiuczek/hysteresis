#ifndef __chiu_rk_step__
#define __chiu_rk_step__

#include "utility.h"
#include <iostream>
#include <cmath>

class rkSolver
{
protected:
//public:
	double y,t,ynew,yerr,eps,yscal,tauLast,tauNext,tauOrg,dydt;
	pfn2 rhs;		//	Pointer to function containing right hand
					//	side of ode in the form rhs(t,y).
	void rkInt(double h);
	static double tauMax;
	static double tauMin;
public:
	rkSolver(const double y0,const double t0,const double tau0,const double acc,pfn2 rhsFn)
		:y(y0),t(t0),tauLast(tau0),tauNext(tau0),tauOrg(tau0),rhs(rhsFn),eps(acc)
	{
		dydt = rhs(t,y);
	};
	
	void rkStep();
	virtual void step();
};

#endif