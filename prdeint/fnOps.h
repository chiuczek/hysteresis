#ifndef __chiu_fnOps__
#define __chiu_fnOps__

#include <cmath>
#include "baseOps.h"
#include "opTemplates.h"
#include "utility.h"

using namespace std;

struct fn1d
{
	pfn1 fn;
	
	fn1d(pfn1 _f):fn(_f){};
	
	double operator()(const double a)
	{
		return fn(a);
	};
	
	double operator()(const double a,const double b)
	{
		return fn(a);
	};
	
	template<class FN>
	nestedFn<fn1d,FN> operator()(FN inner)
	{
		return nestedFn<fn1d,FN>(*this,inner);
	};
};

struct fn2d
{
	pfn2 fn;
	
	fn2d(pfn2 _f):fn(_f){};
	
	double operator()(const double a,const double b)
	{
		return fn(a,b);
	};
	
	template<class FN>
	nestedFn<fn2d,FN> operator()(FN inner)
	{
		return nestedFn<fn2d,FN>(*this,inner);
	};
};


template<class Left,class Op>
struct fnOp1<Left,Op,fn1d>
{
	Left lfn;
	fn1d rfn;
	
	fnOp1(Left _l,fn1d _r):lfn(_l),rfn(_r){};
	
	double operator()(double a)
	{
		return Op::apply(lfn(a),rfn(a));
	}
	double operator()(double a, double b)
	{
		return Op::apply(lfn(a,b),rfn(a));
	}
};

template<class Op>
struct fnOp1<double,Op,fn1d>
{
	double xval;
	fn1d rfn;
	
	fnOp1(double _x,fn1d _r):xval(_x),rfn(_r){};
	
	double operator()(double a)
	{
		return Op::apply(xval,rfn(a));
	}
	double operator()(double a, double b)
	{
		return Op::apply(xval,rfn(a));
	}
};

template<class Inner,class Outer>
struct outerFn
{
	Inner left;
	Outer right;
	
	outerFn(Inner lfn,Outer rfn):left(lfn),right(rfn){};
	
	double operator()(double a,double b)
	{
		return left(a) * right(b);
	};
	
	template<class FN>
	nestedFn<outerFn,FN> operator()(FN inner)
	{
		return nestedFn<outerFn,FN>(*this,inner);
	};
};





template<class Inner>
struct nestedFn<fn1d,Inner>
{
	fn1d f;
	Inner g;
	
	nestedFn(fn1d _f,Inner _g):f(_f),g(_g){};
	
	double operator()(double a)
	{
		return f( g(a) );
	};
	
	double operator()(double a,double b)
	{
		return f( g(a,b) );
	};
};

template<>
struct nestedFn<fn2d,fn1d>
{
	fn2d f;
	fn1d g;
	
	nestedFn(fn2d _f,fn1d _g):f(_f),g(_g){};
	
	double operator()(double a,double b)
	{
		return f( g(a), b );
	};
};

double retZero(double a,double b);
double retZero(double t);
double oneFn(double t);
template<class Inner,class Outer>
outerFn<Inner,Outer> outer(Inner left, Outer right);
template<class Right>
fnOp1<fn1d,Subtract,Right> operator-(Right a);


#endif