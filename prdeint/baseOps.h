#ifndef __chiu_baseOps__
#define __chiu_baseOps__

struct Multiply
{
	static double apply(double a, double b)
	{
		return a*b;
	}
};

struct Divide
{
	static double apply(double a, double b)
	{
		return a/b;
	}
};

struct Add
{
	static double apply(double a, double b)
	{
		return a+b;
	}
};

struct Subtract
{
	static double apply(double a, double b)
	{
		return a-b;
	}
};

#endif