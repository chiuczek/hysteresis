#include "soilEnergyCalculator.h"
#include <fstream>

using namespace std;

double triIntPar;

double soilEnergyCalculator::energy(double input)
{
	int i = arrl - 1;
	int j = wouldbepos(input) - 1;

	double res;

	double temp = (j-1 < 0) ? lbound : arr[j-1];

	res = energyTriangle(input,temp);
	int sg = -1;

	while(j <= i)
	{
		temp = (j-1 < 0) ? lbound : arr[j-1];
		res += sg*energyTriangle(temp,arr[j]);
		j++;
		sg *= -1;
	}

	return res;
}

double soilEnergyCalculator::energyTriangle(double x1, double x2)
{
	double l = Min(x1,x2);
	double r = Max(x1,x2);

	double c1 = c*(l-rbound) + rbound;
	double c2 = (r-rbound)/c + rbound;
	double res;

	if(l >= rbound || r <= lbound)
		return 0.0;

	if(r > c1)
	{
		res = rombInt(energyIntegrandTrap,l,c2,1e-8);
		triIntPar = r;
		if( r > c2 )
            res += rombInt(energyIntegrandTri,c2,r,1e-8);
	}
	else
	{
		triIntPar = r;
		res = rombInt(energyIntegrandTri,l,r,1e-8);
	}
	return res;
}

double energyIntegrandTrap(double alpha)
{
	double x = (alpha-1.0)/hg;
	double t1 = pow(x,nm);
	double t2 = pow(1.0+t1,mm+1.0);

	return bigD*t1/t2;
}

double energyIntegrandTri(double alpha)
{
	double x = (alpha-1.0)/hg;
	double t1 = pow(x,nm);
	double t2 = pow(1.0+t1,-1-mm);

	return bigC*(triIntPar-alpha)*(triIntPar-alpha)*t2*t1/(x*x);
}
