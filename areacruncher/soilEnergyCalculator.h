#pragma once
#include "soilPreisach.h"
#include "kerrysoil.h"

class soilEnergyCalculator :
	public soilPreisach
{
public:
	soilEnergyCalculator(const double &lb,const double &rb):soilPreisach(lb,rb){};
	soilEnergyCalculator():soilPreisach(){};
	soilEnergyCalculator(const double *pts, const int ptsl,
		const double lb,const double rb):soilPreisach(pts,ptsl,lb,rb){};
	soilEnergyCalculator(const absPreisach &p):soilPreisach(p){};
	~soilEnergyCalculator(void) {};

	virtual absPreisach* Clone() { return new soilEnergyCalculator(*this); }

	double energy(double input);
//private:
	double energyTriangle(double x1, double x2);
};

double energyIntegrandTrap(double alpha);
double energyIntegrandTri(double alpha);

const double bigC = -1.0*(mm*nm)/(2.0*(c-1.0)*hg*hg);
const double bigD = -0.5*mm*nm*(c-1.0);
