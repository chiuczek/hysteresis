#include "utility.h"
#include "csvParser.h"
#include "soilEnergyCalculator.h"
#include <iostream>
#include <fstream>
#include <cmath>
#include <string>

using namespace std;

double scaler(double x)
{
	return 1 + 4*x;
}

int main()
{
	csvParser dat1;

	string inFile;

	cout << "Please enter the input filename: ";
	cin >> inFile;
	cout << "Thank you!" << el;

	dat1.parse(inFile.c_str());

	inFile += ".out.csv";

	ofstream fout(inFile.c_str());
	fout.precision(8);

    double init = 1.0;
	soilEnergyCalculator calc1(-10.0,1.0);
	calc1.add(init);

	int noPts = dat1.noOfRows();
	int noCpts = dat1.noOfColumns();

	double totEnergy = 0, stepEnergy;

	calc1.add(scaler(dat1(0,1)));

	fout << "T,X,Y,dE,E" << el;
	fout << dat1(0,0) << sep << dat1(0,1) << sep << dat1(0,3)
            << sep << 0 << sep << 0 << el;

    totEnergy = 0.0;

	for(int i = 1; i < noPts - 1; i++)
	{
		stepEnergy = calc1.energy(scaler(dat1(i,1)));
		totEnergy += stepEnergy;
		calc1.add(scaler(dat1(i,1)));
		fout << dat1(i,0) << sep << dat1(i,1) << sep << dat1(i,3)
			<< sep << stepEnergy << sep << totEnergy << el;
	}
}
