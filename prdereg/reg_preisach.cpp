#include "reg_preisach.h"

double reg_preisach::inverse(const double& _out) const
{
	if( out_max + reg_par*in_right <= _out )
		return (_out - out_max)/reg_par;
	else if( out_min + reg_par*in_left >= _out )
		return (_out - out_min)/reg_par;

	return bisect(_out,in_left,in_right,*this);
}

double reg_preisach::pr_output() const
{
	if( !normalised )
		return pr_state->output();
	else
		return out_min + scale_factor*(pr_state->output()+offset);
}

double reg_preisach::pr_output(const double& _in) const
{
	if( !normalised )
		return pr_state->output(_in);
	else
		return out_min + scale_factor*(pr_state->output(_in)+offset);
}

void reg_preisach::normalise(const double& new_out_min, const double& new_out_max)
{
	if( !normalised )
	{
		scale_factor = fabs(new_out_max-new_out_min)/fabs(out_max-out_min);
		offset = -out_min;
		out_min = new_out_min;
		out_max = new_out_max;
		normalised = true;
	}
	else
	{
		scale_factor = scale_factor*fabs(new_out_max-new_out_min)/fabs(out_max-out_min);
		out_min = new_out_min;
		out_max = new_out_max;
	}
}
