#pragma once
#include "utility.h"
#include "reg_preisach.h"
#include <iostream>

//	Estimates solution of equation: x'(t) = f(y,t)
//	with x = (P+eI)y
class PrDEreg
{
public:
	PrDEreg(const double& y_0, const double& t_0, const double& _eps, const double& _dt,
		absPreisach& init_state, pfn2 _rhs);
	~PrDEreg(void);
//	The () operator will evaluate the right hand side of the equation, for use in numerical algorithm
	inline double operator()(const double& _x, const double& _t) const;
	double step(void);
	double step(const double& _dt);
	friend ostream& operator<<(std::ostream& out, const PrDEreg &prde)
	{
		ostringstream temp;
		temp << prde.t_cur << sep
			<< prde.x_cur << sep
			<< prde.y_cur;
		return (out << temp.str());
	}
	inline ostream& print_head(std::ostream& out)
	{
		return ( out << "t,x,y" );
	}
	double t (void) const { return t_cur; }
	double y (void) const { return y_cur; }
	double x (void) const { return x_cur; }
	void normalise(const double& new_out_min, 
		const double& new_out_max) { r_state.normalise(new_out_min,new_out_max); }
	void reset(const double& _y0, const double& _t0);
private:
	reg_preisach r_state;
	double x_cur;
	double y_cur;
	double t_cur;
	double step_size;
	pfn2 rhs;
	rkBridge<PrDEreg> int_bridge;
};
