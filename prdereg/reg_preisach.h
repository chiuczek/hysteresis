#pragma once
#include "absPreisach.h"
#include "utility.h"

class reg_preisach
{
public:
	reg_preisach(absPreisach& init_state, const double& _eps) : reg_par(_eps), normalised(false)
	{
		pr_state = init_state.Clone();
		in_left = pr_state->getMin();
		in_right = pr_state->getMax();
		out_min = pr_state->output( in_left - 1.0 );
		out_max = pr_state->output( in_right + 1.0 );
	};
	~reg_preisach(void) { delete pr_state; };

	double output() const { return pr_output() + reg_par*pr_state->lastPoint(); }
	double output(const double& _in) const { return pr_output(_in) + reg_par*_in; }

	double pr_output() const;
	double pr_output(const double& _in) const;

	bool add(const double _in) { return pr_state->add(_in); }
	double inverse(const double& _out) const;
	inline double operator()(const double& _in) const { return output(_in); }
	void normalise(const double& new_out_min, const double& new_out_max);
	bool reset(const double& _in) { pr_state->add(in_left-1); return pr_state->add(_in); }
private:
	absPreisach* pr_state;
	double reg_par;
	double out_min;
	double out_max;
	double in_left;
	double in_right;
	bool normalised;
	double scale_factor;
	double offset;
};
