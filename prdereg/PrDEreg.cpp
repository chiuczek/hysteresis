#include "PrDEreg.h"

PrDEreg::PrDEreg(const double& y_0, const double& t_0, const double& _eps, const double& _dt,
		absPreisach& init_state, pfn2 _rhs) 
	: y_cur(y_0)
	, t_cur(t_0)
	, step_size(_dt)
	, r_state(init_state,_eps)
	, int_bridge(y_0,t_0,*this)
	, rhs(_rhs)
{
//	r_state.normalise(0.0,5.0);
	r_state.add(y_cur);
	x_cur = r_state.output();
	int_bridge.update(x_cur,t_cur);
}

PrDEreg::~PrDEreg(void)
{
}

void PrDEreg::reset(const double& _y0, const double& _t0)
{
	y_cur = _y0;
	r_state.reset(y_cur);
	x_cur = r_state.output();
	t_cur = _t0;
	int_bridge.update(x_cur,t_cur);
}

double PrDEreg::operator()(const double& _x, const double& _t) const
{
	double _y = r_state.inverse(_x);
	return rhs(_y,_t);
}

//	Perform one step of the integration.
//	Data member step_size determines the step size. 
//	New values produced will be x,y,t (t=t+dt).
//	Update Preisach state with new value of y.
//	Return x
double PrDEreg::step(void)
{
	rk4Int(step_size,int_bridge);

	x_cur = int_bridge.ret_dep();
	t_cur = int_bridge.ret_ind();
	y_cur = r_state.inverse(x_cur);

	int_bridge.update(x_cur,t_cur);
	r_state.add(y_cur);
	return x_cur;

/*	double y_new, t_new;
	rk4Int(step_size,int_bridge);
	y_new = int_bridge.retY();
	t_new = int_bridge.retT();

	y_cur = y_new;
	t_cur = t_new;

	int_bridge.update(y_cur,t_cur);
	r_state.add(y_cur);
	return y_cur; */
}

double PrDEreg::step(const double& _dt)
{
	step_size = _dt;
	return step();
}