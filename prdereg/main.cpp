#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include "utility.h"
#include "PrDEreg.h"

using namespace std;

double r_h_s(double _y, double _t);
double in_func(const double& _t);
//double switched(const double& _t);
double k = 0.05;
//double pre_lev = 1;
double post_lev = 1.0;
double trough = 0.0;

//bool jump = false;
//double jump_val;

int main(void)
{
	unPreisach test_state(-2.0,2.0);
	test_state.add(0.0);
	double y, t = 0.0, dt = 0.005, reg_par = 0.001;
	int noTests = 6;
//	vector<double> jump_vals(noTests+1,0);
	y = in_func(t);

	PrDEreg* init_eqn;

	vector<PrDEreg*> eqns;
	vector<double> epses;

	for(int i = 0; i <= noTests; i++)
	{
		init_eqn = new PrDEreg(y,t,reg_par,dt,test_state,r_h_s);
		eqns.push_back(init_eqn);
		epses.push_back(reg_par);
		eqns[i]->normalise(0.0,2.0);
		eqns[i]->reset(y,t);
		reg_par *= 0.1;
	}

	ofstream fout("test.csv");
	fout.precision(14);
	fout.setf(ios_base::fixed,ios_base::floatfield);

	fout << "t,";
	for(int i = 0; i <= noTests; i++)
	{
		fout << "x(" << epses[i] << "),y(" << epses[i] << "),";
	}
	fout << "I" << el;

	while( t <= 50.0 )
	{
		fout << t << sep;
		for(int i = 0; i <= noTests; i++)
		{
//			if( jump )
//				jump_val = jump_vals[i];
			eqns[i]->step();
			fout << eqns[i]->x() << sep << eqns[i]->y() << sep;
		}
		fout << in_func(t) << el;
		t += dt;
//		if( !jump && t >= 23 )
//		{
//			for(int i = 0; i <= noTests; i++)
//			{
//				jump_vals[i] = eqns[i]->y();
//			}
//			jump = true;
//		}
	}

	for(int i = 0; i <= noTests; i++)
		delete eqns[i];
}

double r_h_s(double _y, double _t)
{
	return k*(in_func(_t) - _y);
}

//*
double in_func(const double& _t)
{
//	if( jump )
//		return jump_val;
	if( _t <= 20.0 )
		return 1.0;
	else if( _t <= 25.0 )
		return trough + (trough - 1.0)*(_t - 25.0)/5.0;
	else if( _t <= 30.0 )
		return post_lev + (post_lev - trough)*(_t - 30)/5.0;
	else return post_lev;
}
//*/
/*
double in_func(const double& _t)
{
	if( _t <= 20.0 )
		return 1.0;
	else if( _t <= 30 )
		return 1.0 + (post_lev - 1.0)*(_t - 20)/10.0;
	else return post_lev;
}
*/